package list;

// lista circolare bilinkata con una sentinella
public class List<T> {
    protected Pos head;  // head sentinel

    public class Pos {
        protected T val;
        protected Pos pred;
        protected Pos succ;

        public Pos(T v) {
            this.val = v;
            pred = succ = null;
        }
    }


    public List () {
        this.head = new Pos(null);
        this.head.pred = this.head.succ = this.head;
    }

    public boolean empty() {
        return this.head.succ == this.head;
    }

    public Pos head() {
        return this.head.succ;
    }

    public Pos tail() {
        return this.head.pred;
    }

    public Pos next(Pos p) {
        return p.succ;
    }

    public Pos prev(Pos p) {
        return p.pred;
    }

    public boolean finished(Pos p) {
        return p == this.head;
    }

    public int read(Pos p) {
        return (p.val != null ? Integer.parseInt(p.val.toString()) : 0);
    }

    public void write(Pos p, T v) {
        p.val = v;
    }

    // inserisce l'elemento alla posizione p, shiftando da p in poi di una posizione
    public Pos insert(Pos p, T v) {
        Pos t = new Pos(v);
        t.pred = p.pred;
        t.pred.succ = t;
        t.succ = p;
        p.pred = t;
        return t;
    }

    public Pos remove(Pos p) {
        p.pred.succ = p.succ;
        p.succ.pred = p.pred;
        Pos t = p.succ;
        p = null;  // delete element to remove
        return t;
    }

    @Override
    public String toString() {
        String result = "";
        Pos p = this.head();
        while (!this.finished(p)) {
            result += (p.val != null ? p.val.toString() : "") + " ";
            p = this.next(p);
        }
        return result;
    }

    public T getElem(Pos p) {
        return p.val;
    }
}