package tree;

public class RedBlackTree<T> {
    protected int key;
    protected T value;
    protected RedBlackTree<T> parent;
    protected RedBlackTree<T> left;
    protected RedBlackTree<T> right;
    protected COLOR color;

    public enum COLOR { RED, BLACK };

    RedBlackTree(int k, T v) {
        this.key = k;
        this.value = v;
        this.parent = null;
        this.left = null;
        this.right = null;
        this.color = null;
    }

    public RedBlackTree<T> left() {
        return this.left;
    }

    public RedBlackTree<T> right() {
        return this.right;
    }

    // Costo: O(h)  con h = altezza dell'albero
    public RedBlackTree<T> lookupNode(RedBlackTree<T> T, int x) {
        while (T != null && T.key != x) {
            T = (x < T.key ? T.left : T.right);
        }
        return T;
    }

    // Costo: O(1)
    protected void link(RedBlackTree<T> p, RedBlackTree<T> u, int x) {
        if (u != null) u.parent = p;
        if (p != null) {
            if (x < p.key) p.left = u;
            else p.right = u;
        }
    }

    // Costo: O(h)
    @SuppressWarnings("unused")
    protected void balanceInsert(RedBlackTree<T> t) {
        t.color = COLOR.RED;
        while (t != null) {
            RedBlackTree<T> p = t.parent;
            RedBlackTree<T> n = (p.parent != null ? p.parent : null);
            RedBlackTree<T> z = (n == null ? null : (n.left == p ? n.right : n.left));
            if (p == null) {
                t.color = COLOR.BLACK;
                t = null;
            } else if (p.color == COLOR.BLACK) {
                t = null;
            } else if (z.color == COLOR.RED) {
                p.color = z.color = COLOR.BLACK;
                n.color = COLOR.RED;
                t = n;
            } else {
                if (t == p.right && p == n.left) {
                    rotateLeft(p);
                    t = p;
                } else if (t == p.left && p == n.right) {
                    rotateRight(p);
                    t = p;
                } else {
                    if (p == t.left && t == n.left) {
                        rotateRight(n);
                    } else if (p == t.right && t == n.right) {
                        rotateLeft(n);
                    }
                    p.color = COLOR.BLACK;
                    n.color = COLOR.RED;
                    t = null;
                }
            }
        }
    }

    // Costo: O(h)
    public RedBlackTree<T> insertNode(RedBlackTree<T> t, int x, T v) {
        RedBlackTree<T> p = null;
        RedBlackTree<T> u = t;
        while (u != null && u.key != x) {  // Cerca posizione inserimento
            p = u;
            u = (x < u.key ? u.left : u.right);
        }
        if (u != null && u.key == x) {  // Chiave gia' presente
            u.value = v;
        } else {
            RedBlackTree<T> n = new RedBlackTree<T>(x, v);
            link(p, n, x);
            balanceInsert(n);
            if (p == null)
                return n;  // Primo nodo ad essere inserito
        }
        return t;  // Ritorna albero non modificato
    }

    protected void balanceDelete(RedBlackTree<T> root, RedBlackTree<T> t) {
        while (t != root && t.color == COLOR.BLACK) {
            RedBlackTree<T> p = t.parent;  // Padre
            if (t == p.left()) {
                RedBlackTree<T> f = p.right;  // Fratello
                RedBlackTree<T> ns = f.left;  // Nipote sinistro
                RedBlackTree<T> nd = f.right;  // Nipote destro
                if (f.color == COLOR.RED) {
                    p.color = COLOR.RED;
                    f.color = COLOR.BLACK;
                    rotateLeft(p);
                } else {
                    if (ns.color == nd.color && ns.color == COLOR.BLACK) {
                        f.color = COLOR.RED;
                        t = p;
                    } else if (ns.color == COLOR.RED && nd.color == COLOR.BLACK) {
                        ns.color = COLOR.BLACK;
                        f.color = COLOR.RED;
                        rotateRight(f);
                    } else if (nd.color == COLOR.RED) {
                        f.color = p.color;
                        p.color = COLOR.BLACK;
                        nd.color = COLOR.BLACK;
                        rotateLeft(p);
                        t = root;
                    }
                }
            } else {
                RedBlackTree<T> f = p.left;  // Fratello
                RedBlackTree<T> ns = f.right;  // Nipote sinistro
                RedBlackTree<T> nd = f.left;  // Nipote destro
                if (f.color == COLOR.RED) {
                    p.color = COLOR.RED;
                    f.color = COLOR.BLACK;
                    rotateLeft(p);
                } else {
                    if (ns.color == nd.color && ns.color == COLOR.BLACK) {
                        f.color = COLOR.RED;
                        t = p;
                    } else if (ns.color == COLOR.RED && nd.color == COLOR.BLACK) {
                        ns.color = COLOR.BLACK;
                        f.color = COLOR.RED;
                        rotateLeft(f);
                    } else if (nd.color == COLOR.RED) {
                        f.color = p.color;
                        p.color = COLOR.BLACK;
                        nd.color = COLOR.BLACK;
                        rotateRight(p);
                        t = root;
                    }
                }
            }
        }
        if (t != null) t.color = COLOR.BLACK;
    }

    // Costo: O(h)
    public RedBlackTree<T> removeNode(RedBlackTree<T> t, int x) {
        RedBlackTree<T> u = lookupNode(t, x);
        if (u != null) {
            if (u.left != null && u.right != null) {
                RedBlackTree<T> s = u.right;
                while (s.left != null) s = s.left;
                u.key = s.key;
                u.value = s.value;
                u = s;
            }
            RedBlackTree<T> temp;
            if (u.left != null && u.right == null) temp = u.left;
            else temp = u.right;
            link(u.parent, temp, x);
            if (u.color == COLOR.BLACK) balanceDelete(t, temp);
            if (u.parent == null) t = temp;
            u = null;  // Cancella u
        }
        return t;
    }

    // Costo: O(h)
    public RedBlackTree<T> min(RedBlackTree<T> t) {
        while (t.left != null) {
            t = t.left;
        }
        return t;
    }

    // Costo: O(h)
    public RedBlackTree<T> max(RedBlackTree<T> t) {
        while (t.right != null) {
            t = t.right;
        }
        return t;
    }

    // Costo: O(n)
    public RedBlackTree<T> successorNode(RedBlackTree<T> t) {
        if (t == null) {
            return t;
        }
        if (t.right != null) {
            return min(t.right());
        }
        RedBlackTree<T> p = t.parent;
        while (p != null && t == p.right) {
            t = p;
            p = p.parent;
        }
        return p;
    }

    // Costo: O(n)
    public RedBlackTree<T> predecessorNode(RedBlackTree<T> t) {
        if (t == null) {
            return t;
        }
        if (t.left != null) {
            return max(t.left());
        }
        RedBlackTree<T> p = t.parent;
        while (p != null && t == p.left) {
            t = p;
            p = p.parent;
        }
        return p;
    }

    public RedBlackTree<T> rotateLeft(RedBlackTree<T> x) {
        RedBlackTree<T> y = (RedBlackTree<T>) x.right;
        RedBlackTree<T> p = (RedBlackTree<T>) x.parent;
        x.right = y.left;
        if (y.left != null) y.left.parent = x;
        y.left = x;
        x.parent = y;
        y.parent = p;
        if (p != null)
            if (p.left == x) p.left = y;
            else p.right = y;
        return y;
    }

    public RedBlackTree<T> rotateRight(RedBlackTree<T> x) {
        RedBlackTree<T> y = (RedBlackTree<T>) x.left;
        RedBlackTree<T> p = (RedBlackTree<T>) x.parent;
        x.left = y.right;
        if (y.right != null) y.right.parent = x;
        y.right = x;
        x.parent = y;
        y.parent = p;
        if (p != null)
            if (p.right == x) p.right = y;
            else p.left = y;
        return y;
    }
}
