package tree;

public class BinaryTreeFunctions<T> {

    public void visitaProfonditaAnticipata(BinaryTree<T> t) {
        if (t != null) {
            System.out.println(t.read());
            visitaProfonditaAnticipata(t.left());
            visitaProfonditaAnticipata(t.right());
        }
    }

    public void visitaProfonditaSimmetrica(BinaryTree<T> t) {
        if (t != null) {
            visitaProfonditaSimmetrica(t.left());
            System.out.println(t.read());
            visitaProfonditaSimmetrica(t.right());
        }
    }

    public void visitaProfonditaPosticipata(BinaryTree<T> t) {
        if (t != null) {
            visitaProfonditaPosticipata(t.left());
            visitaProfonditaPosticipata(t.right());
            System.out.println(t.read());
        }
    }
}
