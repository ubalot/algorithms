package tree;

public class HashTree<T> {
    public int key;
    public T value;
    public HashTree<T> parent;
    public HashTree<T> left;
    public HashTree<T> right;

    public HashTree(int k, T v) {
        this.key = k;
        this.value = v;
        this.parent = null;
        this.left = null;
        this.right = null;
    }

    public HashTree<T> left() {
        return this.left;
    }

    public HashTree<T> right() {
        return this.right;
    }

    // Costo: O(h)  con h = altezza dell'albero
    public HashTree<T> lookupNode(HashTree<T> T, int x) {
        while (T != null && T.key != x) {
            T = (x < T.key ? T.left : T.right);
        }
        return T;
    }

    // Costo: O(1)
    public void link(HashTree<T> p, HashTree<T> u, int x) {
        if (u != null) u.parent = p;
        if (p != null) {
            if (x < p.key) p.left = u;
            else p.right = u;
        }
    }

    // Costo: O(h)
    public HashTree<T> insertNode(HashTree<T> t, int x, T v) {
        HashTree<T> p = null;
        HashTree<T> u = t;
        while (u != null && u.key != x) {  // Cerca posizione inserimento
            p = u;
            u = (x < u.key ? u.left : u.right);
        }
        if (u != null && u.key == x) {  // Chiave gia' presente
            u.value = v;
        } else {
            HashTree<T> n = new HashTree<T>(x, v);
            link(p, n, x);
            if (p == null)
                return n;  // Primo nodo ad essere inserito
        }
        return t;  // Ritorna albero non modificato
    }

    // Costo: O(h)
    public HashTree<T> removeNode(HashTree<T> t, int x) {
        HashTree<T> u = lookupNode(t, x);
        if (u != null) {
            if (u.left != null && u.right != null) {
                HashTree<T> s = u.right;
                while (s.left != null) s = s.left;
                u.key = s.key;
                u.value = s.value;
                u = s;
            }
            HashTree<T> temp;
            if (u.left != null && u.right == null) temp = u.left;
            else temp = u.right;
            link(u.parent, temp, x);
            if (u.parent == null) t = temp;
            u = null;  // Cancella u
        }
        return t;
    }

    // Costo: O(h)
    public HashTree<T> min(HashTree<T> t) {
        while (t.left != null) {
            t = t.left;
        }
        return t;
    }

    // Costo: O(h)
    public HashTree<T> max(HashTree<T> t) {
        while (t.right != null) {
            t = t.right;
        }
        return t;
    }

    // Costo: O(n)
    public HashTree<T> successorNode(HashTree<T> t) {
        if (t == null) {
            return t;
        }
        if (t.right != null) {
            return min(t.right());
        }
        HashTree<T> p = t.parent;
        while (p != null && t == p.right) {
            t = p;
            p = p.parent;
        }
        return p;
    }

    // Costo: O(n)
    public HashTree<T> predecessorNode(HashTree<T> t) {
        if (t == null) {
            return t;
        }
        if (t.left != null) {
            return max(t.left());
        }
        HashTree<T> p = t.parent;
        while (p != null && t == p.left) {
            t = p;
            p = p.parent;
        }
        return p;
    }



    // Esercizio 6.2
    public HashTree<T> insertNodeRic(HashTree<T> p, HashTree<T> t, int x, T v) {
        if (t != null) {
            if (x < t.key) {
                return insertNodeRic(t, t.left(), x, v);
            }
            else if (x > t.key) {
                return insertNodeRic(t, t.right(), x, v);
            } else { // t.key == x
                t.value = v;
            }
        } else {
            HashTree<T> u = new HashTree<T>(x, v);
            link(p, u, x);
        }
        return t;
    }
}
