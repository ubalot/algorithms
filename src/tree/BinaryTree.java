package tree;

public class BinaryTree<T> {
    public T value;
    public BinaryTree<T> parent;
    public BinaryTree<T> left;
    public BinaryTree<T> right;

    public BinaryTree(T v) {
        this.parent = null;
        this.left = this.right = null;
        this.value = v;
    }

    public int read() {
        return Integer.parseInt(this.value.toString());
    }

    public void write(T v) {
        this.value = v;
    }

    public BinaryTree<T> parent() {
        return this.parent;
    }

    public BinaryTree<T> left() {
        return this.left;
    }

    public BinaryTree<T> right() {
        return this.right;
    }

    public void insertLeft(BinaryTree<T> T) {
        T.parent = this;
        this.left = T;
    }

    public void insertRight(BinaryTree<T> T) {
        T.parent = this;
        this.right = T;
    }

    public void deleteLeft() {
        if (this.left != null) {
            this.left.deleteLeft();
            this.left.deleteRight();
            this.left = null;
        }
    }

    public void deleteRight() {
        if (this.right != null) {
            this.right.deleteLeft();
            this.right.deleteRight();
            this.right = null;
        }
    }

    public void stampa() {
        if (this != null) {
            System.out.println(this.read());
            if (this.left != null) this.left.stampa();
            if (this.right != null) this.right.stampa();
        }
    }
}
