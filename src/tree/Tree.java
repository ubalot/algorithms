package tree;

import queue.ArrayQueue;
import queue.Queue;

public class Tree<T> {
    private T value;
    private Tree<T> parent;
    private Tree<T> child;
    private Tree<T> sibling;

    public Tree(T val) {
        this.value = val;
        this.parent = this.child = this.sibling = null;
    }

    public int read() {
        return Integer.parseInt(this.value.toString());
    }

    public void write(T val) {
        this.value = val;
    }

    public Tree<T> parent() {
        return this.parent;
    }

    public Tree<T> leftmostchild() {
        return this.child;
    }

    public Tree<T> rightSibling() {
        return this.sibling;
    }

    // inserisce l'elemento in testa alla lista dei figli
    public void insertChild(Tree<T> t) {
        t.parent = this;
        t.sibling = this.child;
        this.child = t;
    }

    // inserisce l'elemento dopo un particolare figlio
    public void insertSibling(Tree<T> t) {
        t.parent = this.parent;
        t.sibling = this.sibling;
        this.sibling = t;
    }

    // Distrugge il sottoalbero radicato nel primo figlio di questo nodo
    public void deleteChild() {
        Tree<T> newChild = this.child.rightSibling();
        this.delete(this.leftmostchild());
        this.child = newChild;
    }

    // Distrugge il sottoalbero radicato nel prossimo fratello di questo nodo
    public void deleteSibling() {
        Tree<T> newBrother = this.sibling.rightSibling();
        this.delete(this.rightSibling());
        this.sibling = newBrother;
    }

    // Distrugge il nodo t e i suoi figli
    public void delete(Tree<T> t) {
        Tree<T> u = t.leftmostchild();
        while(u != null){
            Tree<T> next = u.rightSibling();
            this.delete(u);
            u = next;
        }
    }

    public void print() {
        if (this != null) {
            System.out.print(this.read() + " ");
            if (this.sibling != null) this.sibling.print();
            System.out.println();
            if (this.child != null) this.child.print();
        }
    }

    @SuppressWarnings("unused")
    public void preVisitaProfondita() {
        if (this == null) return ;  //precondition
        System.out.println(this.read());
        Tree<T> u = this.leftmostchild();
        while (u != null) {
            u.preVisitaProfondita();
            u = u.rightSibling();
        }
    }

    @SuppressWarnings("unused")
    public void postVisitaProfondita() {
        if (this == null) return ;  //precondition
        Tree<T> u = this.leftmostchild();
        while(u != null){
            this.postVisitaProfondita();
            u = u.rightSibling();
        }
        System.out.println(this.read());
    }

    @SuppressWarnings("unused")
    public void invisita(int i) {
        if (this == null) return ;  //precondition
        Tree<T> u = this.leftmostchild();
        int k = 0;
        while(u != null && k < i) {
            k++;
            u.invisita(i);
            u = u.rightSibling();
        }
        System.out.println(this.read());
        while (u != null) {
            u.invisita(i);
            u = u.rightSibling();
        }
    }

    public void visitaAmpiezza(Tree<T> t) {
        if (t == null) return ;
        Queue<Tree<T>> Q = new ArrayQueue<Tree<T>>(10);
        Q.enqueue(t);
        while (!Q.isEmpty()) {
            Tree<T> u = Q.dequeue();
            System.out.println(u.read());
            u = u.leftmostchild();
            while (u != null) {
                Q.enqueue(u);
                u = u.rightSibling();
            }
        }
    }
}
