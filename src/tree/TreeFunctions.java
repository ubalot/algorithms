package tree;

import queue.*;

public class TreeFunctions<T> {

    public void preVisitaProfondita(Tree<T> t) {
        if (t == null) return ;  //precondition
        System.out.println(t.read());
        Tree<T> u = t.leftmostchild();
        while (u != null) {
            preVisitaProfondita(u);
            u = u.rightSibling();
        }
    }

    public void postVisitaProfondita(Tree<T> t) {
        if (t == null) return ;  //precondition
        Tree<T> u = t.leftmostchild();
        while(u != null){
            postVisitaProfondita(u);
            u = u.rightSibling();
        }
        System.out.println(t.read());
    }

    public void invisita(Tree<T> t, int i) {
        if (t == null) return ;  //precondition
        Tree<T> u = t.leftmostchild();
        int k = 0;
        while(u != null && k < i) {
            k++;
            invisita(u, i);
            u = u.rightSibling();
        }
        System.out.println(t.read());
        while (u != null) {
            invisita(u, i);
            u = u.rightSibling();
        }
    }

    public void visitaAmpiezza(Tree<T> t) {
        if (t == null) return ;
        Queue<Tree<T>> Q = new ArrayQueue<Tree<T>>(10);
        Q.enqueue(t);
        while (!Q.isEmpty()) {
            Tree<T> u = Q.dequeue();
            System.out.println(u.read());
            u = u.leftmostchild();
            while (u != null) {
                Q.enqueue(u);
                u = u.rightSibling();
            }
        }
    }
}