package flag;

import java.util.Random;

public class Flag {

    public enum COLOR {VERDE, BIANCO, ROSSO};

    public static COLOR[] generaArrayBandiera(int n) {
        COLOR[] A = new COLOR[n];
        Random randomGenerator = new Random();
        for (int i = 0; i < n; i++) {
            switch (randomGenerator.nextInt(3)) {
                case 0: A[i] = COLOR.VERDE; break;
                case 1: A[i] = COLOR.BIANCO; break;
                case 2: A[i] = COLOR.ROSSO; break;
                default: System.out.println("Errore nella generazione di ArrayBandiera"); break;
            }
        }
        return A;
    }

    public static void ordinaBandiera(COLOR[] B, int n) {
        int k = 0;
        int j = n - 1;
        while (k < n && B[k] == COLOR.VERDE) k++;
        while (j >= 0 && B[j] == COLOR.ROSSO) j--;
        int i = k;
        while (i <= j) {
            if (B[i] == COLOR.ROSSO) {
                swap(B, i, j);
                j--;
            } else if (B[i] == COLOR.VERDE) {
                swap(B, i, k);
                k++;
            }
            if (B[i] == COLOR.BIANCO) i++;
        }
    }
    // Funzione ausiliaria per ordinaBandiera()
    private static void swap(COLOR[] A, int pos1, int pos2) {
        COLOR temp = A[pos1];
        A[pos1] = A[pos2];
        A[pos2] = temp;
    }

    public static void stampa(COLOR[] A) {
        int verde = 0, bianco = 0, rosso = 0;
        for (COLOR c : A) {
            switch (c) {
                case VERDE: verde++; break;
                case BIANCO: bianco++; break;
                case ROSSO: rosso++; break;
            }
            System.out.print(c.toString() + " ");
        }
        System.out.println();
        System.out.println("Verde: " + verde);
        System.out.println("Bianco: " + bianco);
        System.out.println("Rosso: " + rosso);
        System.out.println();
    }

}
