package set;

import algorithm.Calculus;

public class BooleanSet {
    protected boolean[] V;
    protected int dim;
    public int capacity;

    public BooleanSet(int N) {
        dim = 0;
        capacity = N;
        V = new boolean[N];
        for (int i = 0; i < N; i++) V[i] = false;
    }

    // Costo: O(1)
    public int size() {
        return dim;
    }

    // Costo: O(1)
    public boolean isEmpty() {
        return (dim == 0 ? true : false);
    }

    // Costo: O(1)
    public boolean contains(int x) {
        return V[x];
    }

    // Costo: O(1)
    public void insert(int x) {
        if (!V[x]) dim = dim + 1;
        V[x] = true;
    }

    // Costo: O(1)
    public void remove(int x) {
        if (V[x]) dim = dim - 1;
        V[x] = false;
    }

    // Costo: O(N)
    public BooleanSet union(BooleanSet A, BooleanSet B) {
        BooleanSet C = new BooleanSet(Calculus.max(A.capacity, B.capacity));
        for (int i = 0; i < A.capacity; i++) {
            if (A.contains(i)) C.insert(i);
        }
        for (int i = 0; i < B.capacity; i++) {
            if (B.contains(i)) C.insert(i);
        }
        return C;
    }

    // Costo: O(N)
    public BooleanSet intersection(BooleanSet A, BooleanSet B) {
        BooleanSet C = new BooleanSet(Calculus.min(A.capacity, B.capacity));
        for (int i = 0; i < Calculus.min(A.capacity, B.capacity); i++) {
            if (A.contains(i) && B.contains(i)) C.insert(i);
        }
        return C;
    }

    // Costo: O(N)
    public BooleanSet difference(BooleanSet A, BooleanSet B) {
        BooleanSet C = new BooleanSet(A.capacity);
        for (int i = 0; i < A.capacity; i++) {
            if (A.contains(i) && (i > B.capacity || !B.contains(i)))
                C.insert(i);
        }
        return C;
    }
}