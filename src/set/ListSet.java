package set;

import list.List;

// Insieme realizzato con lista non ordinata
public class ListSet<T> {
    public List<T> L;
    protected int dim;

    public ListSet() {
        L = new List<T>();
        dim = 0;
    }

    public int size() {
        return dim;
    }

    public boolean isEmpty() {
        return (dim == 0 ? true : false);
    }

    // Costo: O(n)
    public boolean contains(T elem) {
        List<T>.Pos p = L.head();
        while (!L.finished(p)) {
            if (L.getElem(p) == elem)
                return true;
            p = L.next(p);
        }
        return false;
    }

    // Costo: O(n) per la verifica che l'elemento non sia gia' presente
    public void insert(T elem) {
        if (contains(elem)) return ;
        L.insert(L.prev(L.head()), elem);
        dim = dim + 1;
    }

    // Costo: O(n)
    public T remove(T elem) {
        if (this.contains(elem)) {
            dim = dim - 1;
            List<T>.Pos p = L.head();
            while (!L.finished(p)) {
                if (L.getElem(p) == elem)
                    return L.getElem(L.remove(p));
                p = L.next(p);
            }
        }
        return null;
    }

    // Costo: O(n * m)  con n = |A|, m = |B|
    public ListSet<T> union(ListSet<T> A, ListSet<T> B) {
        ListSet<T> C = new ListSet<T>();
        List<T>.Pos p = A.L.head();
        List<T>.Pos q = B.L.head();
        List<T>.Pos r = C.L.head();
        while (!A.L.finished(p)) {
            C.L.insert(r, A.L.getElem(p));
            p = A.L.next(p);
        }
        while (!B.L.finished(q)) {
            if (!C.contains(B.L.getElem(q)))
                C.L.insert(r, B.L.getElem(q));
            q = B.L.next(q);
        }
        return C;
    }

    // Costo: O(n * m)  con n = |A|, m = |B|
    public ListSet<T> difference(ListSet<T> A, ListSet<T> B) {
        ListSet<T> C = new ListSet<T>();
        List<T>.Pos p = A.L.head();
        List<T>.Pos r = C.L.head();
        while (!A.L.finished(p)) {
            if (!B.contains(A.L.getElem(p))) {
                C.L.insert(r, A.L.getElem(p));
                r = C.L.next(r);
                p = A.L.next(p);
            }
        }
        return C;
    }

    // Costo: O(n * m)  con n = |A|, m = |B|
    public ListSet<T> intersection(ListSet<T> A, ListSet<T> B) {
        ListSet<T> C = new ListSet<T>();
        List<T>.Pos p = A.L.head();
        List<T>.Pos r = C.L.head();
        while (!A.L.finished(p)) {
            if (B.contains(A.L.getElem(p))) {
                C.L.insert(r, A.L.getElem(p));
                r = C.L.next(r);
            }
            p = A.L.next(p);
        }
        return C;
    }

    /*	Il costo di union, intersection e difference puo' essere abbassato a
     *	O(n + m) nel caso la lista sia ordinata
     */
}