package stack;

public interface Stack<T> {
    public boolean isEmpty();
    public void push(T n);
    public T pop();
    public T top();
}
