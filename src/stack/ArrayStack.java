package stack;

// realizzazione pila con vettore
public class ArrayStack<T> implements Stack<T>{
    private T[] A;
    private int n;
    private int m;

    @SuppressWarnings("unchecked")
    public ArrayStack(int dim){
        this.A = (T[]) new Object[dim];
        this.m = dim;
        this.n = 0;
    }

    public T top(){
        if(this.n < 0) return null;  //precondition
        return this.A[this.n];
    }

    public boolean isEmpty(){
        return n == 0;
    }

    public T pop(){
        if(this.n < 0) return null;  //precondition
        T tmp = this.A[this.n];
        this.n--;
        return tmp;
    }

    public void push(T value){
        if(this.n >= this.m) return ;  //precondition
        this.n++;
        this.A[this.n] = value;
    }
}
