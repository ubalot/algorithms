package com.algorithms;

import java.util.ArrayList;
import java.util.Random;

import algorithm.*;
import array.*;
import list.*;
import list.List.Pos;
import priorityqueue.PriorityQueue;
import stack.*;
import queue.*;
import tree.*;
import sequence.*;
import flag.*;
import graph.Graph;


public class Main {

    private static void debug(String s) {
        System.out.println(s);
    }

    public static void stampaHashTree(HashTree<Integer> t) {
        if (t != null){
            System.out.println(t.key + " " + t.value);
            stampaHashTree(t.left());
            stampaHashTree(t.right());
        }
    }
    public static void modifica(List<Integer> L){
        List<Integer>.Pos p = L.head();
        int val = L.read(p);
        boolean b = false;
        while (!L.finished(p)) {
            if (L.read(p) == val + 1 || L.read(p) == val - 1) {
                val = L.read(p);
                if (!b) b = true;
                System.out.println("elemento rimosso: " + L.read(L.prev(p)));
                p = L.remove(L.prev(p));
            } else if (b) {
                b = false;
                System.out.println("elemento rimosso: " + L.read(L.prev(p)));
                p = L.remove(L.prev(p));
            } else {
                p = L.next(p);
            }
        }
    }
    public static void main(String[] args) {
        int[] A = {0,1,2,3,4};
        System.out.println(Search.selezione(A,0,A.length-1,3));
//		List<Integer> L = new List<Integer>();
//	    L.insert(L.head(), 5);
//	    L.insert(L.head(), 2);
//	    L.insert(L.head(), 1);
//	    L.insert(L.head(), 2);
//	    L.insert(L.head(), 3);
//	    L.insert(L.head(), 7);
//	    L.insert(L.head(), 2);
//	    L.insert(L.head(), 6);
//	    L.insert(L.head(), 5);
//	    L.insert(L.head(), 4);

//	    L.insert(L.tail(), 4);
//	    L.insert(L.tail(), 5);
//	    L.insert(L.tail(), 6);
//	    L.insert(L.tail(), 2);
//	    L.insert(L.tail(), 7);
//	    L.insert(L.tail(), 3);
//	    L.insert(L.tail(), 2);
//	    L.insert(L.tail(), 1);
//	    L.insert(L.tail(), 2);
//	    L.insert(L.tail(), 5);
//	    System.out.println(L.toString());
//	    modifica(L);
//	    System.out.println(L.toString());
//	    if (L.empty()) debug("L e' vuota");
//	    debug("fine");


//		int[] A = {10, 4, 8, 7, 6};
//		System.out.println(Search.min(A, A.length));

//		int[] A = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//		System.out.println(Search.binarySearch(A, 4, 0, A.length));

//		int[] A = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//		System.out.println(Search.iterativeBinarySearch(A, 4, A.length));

//		System.out.println(Calculus.moltiplicazioneEgizia(3, 2, 0));

//		int[] A = {10, 4, 8, 7, 6};
//		int[] A = Array.arrayGenerator(10);
//		debug(Array.toString(A));
//		Sorting.stupidSort(A, A.length);
//		Sorting.selectionSort(A, A.length);
//		Sorting.insertionSort(A, A.length);
//		Sorting.mergeSort(A, 0, A.length);
//		Sorting.countingSort(A, A.length, Array.limit);
//		Sorting.sort(A, A.length);
//
//		int[] B = new int[A.length + 1];
//		B[0] = 0; for (int i=0; i<A.length; i++) B[i+1] = A[i];
//		debug(Array.toString(B));
//		Sorting.heapsort(B);
//		debug(Array.toString(B));
//		System.out.println(Sorting.isSorted(B));
//
//		Sorting.quickSort(A, 0, A.length);
//		Sorting.shellSort(A, A.length);
//		debug(Array.toString(A));
//		System.out.println(Sorting.isSorted(A));


        // Esercizio 2.10
//		Bandiera.COLOR[] arrayBandiera = Bandiera.generaArrayBandiera(10);
//		Bandiera.stampa(arrayBandiera);
//		Bandiera.ordinaBandiera(arrayBandiera, arrayBandiera.length);
//		Bandiera.stampa(arrayBandiera);

        // Esercizio 3.6
//		int[] A = {5, 2, 3, 1, 2};
//		Sequence<Integer> S = new Sequence<Integer>();
//		for (int i=0; i<A.length; i++) {
//			S.insert(S.next(S.tail()), A[i]);
//		}
//		System.out.println(S.toString());
//		stampaDifferenze(S);

        // Esercizio 3.7
//		Sequence<Integer> S = new Sequence<Integer>();
//		sequence.Sequence<Integer>.Pos temporary = S.insert(S.tail(), 3);
//		temporary = S.insert(S.tail(), 7);
//		temporary = S.insert(S.tail(), 8);
//		temporary = S.insert(S.tail(), 1);
//		temporary = S.insert(S.tail(), 4);
//		System.out.println(S.toString());
//		precedenzaDispari(S);
//		System.out.println(S.toString());

        // Esercizio 3.8
//		Sequence<Integer> S = new Sequence<Integer>();
//		sequence.Sequence<Integer>.Pos temporary = S.insert(S.tail(), 3);
//		temporary = S.insert(S.tail(), 2);
//		temporary = S.insert(S.tail(), 5);
//		System.out.println(S.toString());
//		rango(S, S.head());
//		System.out.println(S.toString());


        // Esercizio 4.1
//		List<Integer> L = new List<Integer>();
//		List<Integer> M = new List<Integer>();
//		primiN(10, L, M);
//		System.out.println("L: " + L.toString());
//		System.out.println("M: " + M.toString());


        // Esercizio 4.2
//		List<Integer> L = new List<Integer>();
//		int[] A = {5, 7, 3, 2, 2, 1, 2, 3};
//		for (int i=0; i < A.length; i++) {
//			L.insert(L.next(L.tail()), A[i]);
//		}
//		List<Integer> M = epurazione(L);
//		System.out.println("L: " + L.toString());
//		System.out.println("M: " + M.toString());


        // Esercizio 4.3
//		List<Integer> L = new List<Integer>();
//		list.List<Integer>.Pos p = L.insert(L.next(L.tail()), 1);
//		p = L.insert(L.next(L.tail()), 9);
//		p = L.insert(L.next(L.tail()), 0);
//		List<Integer> M = new List<Integer>();
//		p = M.insert(M.next(M.tail()), 1);
//		p = M.insert(M.next(M.tail()), 7);
//		System.out.println("L: " + L.toString());
//		System.out.println("M: " + M.toString());
//		List<Integer> S = somma(L, M);
//		System.out.println("S: " + S.toString());


        // Esercizio 4.5
//		list.List<Integer> L = new List<Integer>();
//		int[] A = {4, 6, 7, 3, 2, 5};
//		for (int i=0; i < A.length; i++) {
//			L.insert(L.next(L.tail()), A[i]);
//		}
//		System.out.println(L.toString());
//		rimuoviPari(L, L.head(), 0);
//		System.out.println(L.toString());


        // Esercizio 4.8
//		List<Integer> L = new List<Integer>();
//		List<Integer> M = new List<Integer>();
//		primiN_iterativa(10, L, M);
//		System.out.println("L: " + L.toString());
//		System.out.println("M: " + M.toString());


        // Esercizio 5.1
//		Tree<Integer> T = new Tree<Integer>(2);
//		T.insertChild(new Tree<Integer>(10));
//		T.leftmostchild().insertSibling(new Tree<Integer>(5));
//		T.leftmostchild().rightSibling().insertChild(new Tree<Integer>(4));
//		T.print();
//		tree.TreeFunctions<Integer> treeFun = new tree.TreeFunctions<Integer>();
//		treeFun.preVisitaProfondita(T);
//		System.out.println("profondita': " + depth(T));

        // Esercizio 5.2
//		Tree<Integer> T = new Tree<Integer>(2);
//		T.insertChild(new Tree<Integer>(10));
//		T.leftmostchild().insertSibling(new Tree<Integer>(5));
//		T.leftmostchild().rightSibling().insertChild(new Tree<Integer>(4));
//		T.print();
//		leafk(T, 11, 0);
//		T.print();

        // Esercizio 5.3
//		Tree<Integer> T = new Tree<Integer>(2);
//		T.insertChild(new Tree<Integer>(10));
//		T.leftmostchild().insertSibling(new Tree<Integer>(5));
//		T.leftmostchild().rightSibling().insertChild(new Tree<Integer>(4));
//		T.leftmostchild().rightSibling().insertChild(new Tree<Integer>(4));
//		T.leftmostchild().rightSibling().insertChild(new Tree<Integer>(4));
//		T.print();
//		System.out.println(width(T));

        // Esercizio 5.7
//		BinaryTree<Integer> T = new BinaryTree<Integer>(1);
//		T.insertLeft(new BinaryTree<Integer>(2));
//		T.insertRight(new BinaryTree<Integer>(3));
//		T.stampa();
//		System.out.println();
//		inserisciFoglia(T);
//		T.stampa();

        // Esercizio 5.8
//		BinaryTree<Integer> T = new BinaryTree<Integer>(0);
//		T.insertLeft(new BinaryTree<Integer>(1));
//		T.insertRight(new BinaryTree<Integer>(1));
//		T.stampa();
//		System.out.println();
//		raggruppa(T);
//		T.stampa();

        // Esercizio 5.9
//		BinaryTree<Integer> root = new BinaryTree<Integer>(0);
//		BinaryTree<Integer> left = new BinaryTree<Integer>(1);
//		left.insertLeft(new BinaryTree<Integer>(2));
//		left.insertRight(new BinaryTree<Integer>(3));
//		BinaryTree<Integer> right = new BinaryTree<Integer>(4);
//		right.insertLeft(new BinaryTree<Integer>(5));
//		right.insertRight(new BinaryTree<Integer>(6));
//		root.insertLeft(left);
//		root.insertRight(right);
//		root.stampa();
//		System.out.println();
//		alberoInverso(root);
//		root.stampa();

        // Esercizio 5.10
//		BinaryTree<Integer> root = new BinaryTree<Integer>(1);
//		root.insertLeft(new BinaryTree<Integer>(4));
//		root.insertRight(new BinaryTree<Integer>(10));
//		root.stampa();
//		System.out.println();
//		addSum(root, 0);
//		root.stampa();

        // Esercizio 6.2
//		HashTree<Integer> T = new HashTree<Integer>(10, 10);
////		T.insertNode(T, 5, 5);
////		stampaHashTree(T);
//		T.insertNodeRic(T, T, 4, 4);
//		stampaHashTree(T);

        // Esercizio 6.3
//		BinaryTree<Integer> t = new BinaryTree<Integer>(10);
//		t.insertLeft(new BinaryTree<Integer>(3));
//		System.out.println(isABR(t));

        // Esercizio 6.4
//		BinaryTree<Integer> root = new BinaryTree<Integer>(10);
//		BinaryTree<Integer> p = new BinaryTree<Integer>(5);
//		BinaryTree<Integer> q = new BinaryTree<Integer>(7);
//		BinaryTree<Integer> r = new BinaryTree<Integer>(12);
//		root.insertLeft(q);
//		q.insertLeft(p);
//		root.insertRight(r);
//		stampaNodi(root, 6, 11);

        // Esercizio 6.6
//		HashTree<Integer> A = new HashTree<Integer>(3, 10);
//		HashTree<Integer> A1 = new HashTree<Integer>(2, 4);
//		A.insertNode(A, A1.key, A1.value);
//		HashTree<Integer> B = new HashTree<Integer>(10, 20);
//		stampaHashTree( concat(A, B));
//		stampaHashTree(A);

//		String s = "cento";
//		int h = Calculus.H(s.toCharArray(), s.length());
//		System.out.println(h);


//		List<Integer> L = new List<Integer>();
//		L.insert(L.next(L.tail()), 5);
//		L.insert(L.next(L.tail()), 4);
//		L.insert(L.next(L.tail()), 3);
//		L.insert(L.next(L.tail()), 2);
//		L.insert(L.next(L.tail()), 1);
//		debug(L.toString());
////		MergeSort(L, L.head(), L.tail());  // Esercizio 12.1
//		QuickSort(L, L.head(), L.tail());  // Esercizio 12.2
//		debug(L.toString());

        // Esercizio 12.3
//		int[] A = Array.arrayGenerator(10);
//		debug(Array.toString(A));
//		System.out.println(Inversioni(A, 0, A.length - 1));

        // Esercizio 12.5
//		int[] A = Array.arrayGenerator(10);
//		debug(Array.toString(A));
//		MergeSortIterativa(A, A.length);
//		debug(Array.toString(A));
//		System.out.println(Sorting.isSorted(A));

        // Esercizio 12.9
//		int[] A = {0, 0, 0, 0, 1};
//		int[] B = {0, 1, 1};
//		int[] C = {0, 0, 0, 0, 0, 0};
//		int[] D = {1, 1, 1, 1, 1};
//		int a0 = conta0(A, 1, A.length - 1);
//		int aH = trovaH(A, A.length - 1);
//		System.out.println("conta0 di A -> " + a0);
//		System.out.println("trovaH di A -> " + aH);
//		int b0 = conta0(B, 0, B.length - 1);
//		int bH = trovaH(B, B.length - 1);
//		System.out.println("conta0 di B -> " + b0);
//		System.out.println("trovaH di B -> " + bH);
//		int c0 = conta0(C, 0, C.length - 1);
//		int cH = trovaH(C, C.length - 1);
//		System.out.println("conta0 di C -> " + c0);
//		System.out.println("trovaH di C -> " + cH);
//		int d0 = conta0(D, 0, D.length - 1);
//		int dH = trovaH(D, D.length - 1);
//		System.out.println("conta0 di D -> " + d0);
//		System.out.println("trovaH di D -> " + dH);

        // Esercizio 12.11
//		int[] A = {0, 1, 3, 4, 5};
//		System.out.println(ricerca(A, 0, A.length));

        // Esercizio 12.12
//		int[] A = {1, 2, 3, 4, 5, 3, 2, 1};
//		System.out.println(maxUnimodale(A, 0, A.length));
//		System.out.println(myMaxUnimodale(A, 0, A.length-1));

        // Esercizio 12.13
//		int[] A = {1, 2, 3, 4, 4, 4, 4, 5, 6};
//		System.out.println(occorrenze(A, 2, A.length));


        // Esercizio 13.6
//	    int[] L = {9, 15, 3, 6, 4, 2, 5, 10, 3};
//	    Sequence<Integer> S = maxSeqCrescente(L, L.length);
//	    System.out.println(S.toString());
    }

    /* CAPITOLO 3 */

    // Esercizio 3.6
    // procedura che stampa tutti gli elementi di una sequenza che sono uguali
    // alla differenza tra l'elemento immediatamente precedente e quello
    // immediatamente seguente. (Si assume che L.length >= 3)
    public static void stampaDifferenze(Sequence<Integer> L){
        sequence.Sequence<Integer>.Pos p = L.head();
        sequence.Sequence<Integer>.Pos q = L.next(p);
        sequence.Sequence<Integer>.Pos r = L.next(q);
        while (!L.finished(r)) {
            if ( (L.read(p) - L.read(r)) ==  L.read(q)) {
                System.out.print(L.read(q) + " ");
            }
            p = q;
            q = r;
            r = L.next(r);
        }
        System.out.println();
    }

    // Esercizio 3.7
    // procedura che riordina tutti gli elementi dispari in modo che precedano,
    // nello stesso ordine che avevano inizialmente in L, tutti gli elementi pari
    public static void precedenzaDispari(Sequence<Integer> L){
        sequence.Sequence<Integer>.Pos p = L.head();
        while (!L.finished(p)) {
            if (L.read(p) % 2 == 0) {
                sequence.Sequence<Integer>.Pos q = L.next(p);
                while (!L.finished(q)) {
                    if (L.read(q) % 2 == 1) {
                        sequence.Sequence<Integer>.Pos aux = L.next(q);
                        sequence.Sequence<Integer>.Pos r = L.remove(q);
                        p = L.insert(p, L.read(r));
                        q = aux;
                    }
                    q = L.next(q);
                }
            }
            p = L.next(p);
        }
    }

    // Esercizio 3.8
    // modifica la sequenza in modo che ogni elemento venga sostituito dal proprio rango
    // Chiamata: rango(L, L.head())
    public static void rango(Sequence<Integer> L, sequence.Sequence<Integer>.Pos p){
        sequence.Sequence<Integer>.Pos q = L.next(p);
        if (!L.finished(q)) {
            rango(L, q);
            L.write(p, L.read(p) + L.read(q));
        }
    }



    /* CAPITOLO 4 */

    // Esercizio 4.1
    // dato n>=1 costruisce due liste L = 1,2,..,n-1,n e M = n,n-1,...,2,1
    public static void primiN(int n, List<Integer> L, List<Integer> M) {
        if (n >= 1) {
            L.insert(L.head(), n);
            M.insert(M.next(M.tail()), n);
            primiN(n-1, L, M);
        }
    }

    // Esercizio 4.2
    // restituisce una lista M che contiene solo gli elementi di L che non
    // compaiono esattamente due volte
    public static List<Integer> epurazione(List<Integer> L) {
        List<Integer> M = new List<Integer>();
        list.List<Integer>.Pos p = L.head();
        while (!L.finished(p)) {
            int conta = 0;
            list.List<Integer>.Pos q = L.head();
            while (!L.finished(q)) {
                if (L.read(p) == L.read(q)) {
                    conta++;
                }
                q = L.next(q);
            }
            if (conta != 2) {
                M.insert(M.next(M.tail()), L.read(p));
            }
            p = L.next(p);
        }
        return M;
    }

    // Esercizio 4.3
    // prende in input due numeri, in forma di lista, e ne restituisce la somma
    public static List<Integer> somma(List<Integer> A, List<Integer> B) {
        List<Integer> S = new List<Integer>();
        list.List<Integer>.Pos p = A.tail();
        while (p != A.prev(A.head())) {
            list.List<Integer>.Pos q = S.insert(S.head(), A.read(p));
            p = A.prev(p);
        }
        p = B.tail();
        list.List<Integer>.Pos q = S.tail();
        boolean riporto = false;
        while (p != B.prev(B.head())) {
            if (riporto) {
                S.write(q, S.read(q) + B.read(p) + 1);
                riporto = false;
            } else {
                S.write(q, S.read(q) + B.read(p));
            }
            if (S.read(q) >= 10) {
                S.write(q, S.read(q) % 10);
                riporto = true;
            }
            p = B.prev(p);
            q = S.prev(q);
        }
        if (riporto)
            S.write(S.head(), S.read(q) + 1);
        return S;
    }

    // Esercizio 4.5
    // rimuove tutti i numeri pari dalla lista e replica ogni elemento dispari tante volte
    // quanti sono i numeri pari che lo precedono
    public static void rimuoviPari(List<Integer> L, list.List<Integer>.Pos p, int c) {
        if (!L.finished(p)) {
            if (L.read(p) % 2 == 0) {
                rimuoviPari(L, L.remove(p), c + 1);
            } else {
                list.List<Integer>.Pos q = p;
                for(int i=1; i<=c; i++)
                    q = L.insert(q,  L.read(p));
                rimuoviPari(L, L.next(p), c);
            }
        }
    }

    // Esercizio 4.8
    public static void primiN_iterativa(int n, List<Integer> L, List<Integer> M) {
        if (n < 1) return ;  // precondition
        list.List<Integer>.Pos p = L.head();
        list.List<Integer>.Pos q = M.tail();
        while (n != 0) {
            p = L.insert(p, n);
            q = M.insert(q, n);
            p = L.next(p);
            n--;
        }
    }



    /* CAPITOLO 5 */

    // Esercizio 5.1
    // restituisce l'altezza di un albero
    public static int depth(Tree<Integer> t) {
        int max = 0;
        Tree<Integer> u = t.leftmostchild();
        while (u != null) {
            int temp = depth(u) + 1;
            if (temp > max) max = temp;
            u = u.rightSibling();
        }
        return max;
    }

    // Esercizio 5.2
    // cancella le foglie con somma k
    // chiamata: leafk(T, k, 0)
    public static boolean leafk(Tree<Integer> t, int k, int somma) {
        somma = somma + t.read();
        if (somma == k && t.leftmostchild() == null) {
            // foglia con valore k, deve essere cancellata
            return true;
        } else {
            while (t.leftmostchild() != null && leafk(t.leftmostchild(), k, somma)) {
                t.deleteChild();
            }
            Tree<Integer> u = t.leftmostchild();  // nodo da cui cancellare
            Tree<Integer> v = (u != null ? u.rightSibling() : null);  // potenziale nodo da cancellare
            while (v != null) {
                if (leafk(v, k, somma)) {
                    u.deleteSibling();  // u rimane fisso e un fratello viene eliminato
                } else {
                    u = v;  // u avanza
                }
                v = u.rightSibling();  // prossimo fratello
            }
            return false;
        }
    }

    // Esercizio 5.3
    // restituisce larghezza di un albero
    public static int width(Tree<Integer> t) {
        if (t == null) return 0;
        int[] A = new int[depth(t) + 1];
        getWidth(t, A, 0);
        int max = A[0];
        for (int i = 0; i < A.length; i++)
            if (A[i] > max) max = A[i];
        return max;
    }
    public static void getWidth(Tree<Integer> t, int[] A, int l) {
        if (t == null) return ;
        A[l]++;
        Tree<Integer> u = t.leftmostchild();
        while (u != null) {
            getWidth(u, A, l + 1);
            u = u.rightSibling();
        }
    }

    // Esercizio 5.7
    // inserisce nuova foglia contenente uno 0 come figlio sinistro di ogni foglia dell'albero binario
    public static void inserisciFoglia(BinaryTree<Integer> t) {
        if (t != null) {
            inserisciFoglia(t.left());
            inserisciFoglia(t.right());
            if (t.left() == null && t.right() == null) {
                BinaryTree<Integer> n = new BinaryTree<Integer>(0);
                t.insertLeft(n);
            }
        }
    }

    // Esercizio 5.8
    public static void raggruppa(BinaryTree<Integer> t) {
        if (t != null) {
            raggruppa(t.left());
            if (t.left() != null && t.left().read() == 1) {
                if (t.right().read() == 0) {
                    t.right().write(1);
                    t.left().write(0);
                } else if (t.read() == 0) {
                    t.write(1);
                    t.left().write(0);
                }
            }
            raggruppa(t.right());
            if (t.right() != null && t.read() == 1) {
                if (t.right().read() == 0) {
                    t.right().write(1);
                    t.write(0);
                }
            }
        }
    }

    // Esercizio 5.9
    public static void alberoInverso(BinaryTree<Integer> t) {
        if (t != null) {
            if (t.left() != null) alberoInverso(t.left());
            if (t.right() != null) alberoInverso(t.right());
            BinaryTree<Integer> u = t.left();
            BinaryTree<Integer> v = t.right();
            if (v != null) t.insertLeft(v);
            if (u != null) t.insertRight(u);
        }
    }

    // Esercizio 5.10
    public static void addSum(BinaryTree<Integer> t, int somma) {
        if (t != null) {
            somma = somma + t.read();
            addSum(t.left(), somma);
            addSum(t.right(), somma);
            if (t.left() == null && t.right() == null) {
                BinaryTree<Integer> u = new BinaryTree<Integer>(somma);
                t.insertLeft(u);
            }
        }
    }



    /* CAPITOLO 6 */

    // Esercizio 6.2
    // funzione definita nel package tree.HashTree

    // Esercizio 6.3
    public static boolean isABR(BinaryTree<Integer> t) {
        if (t != null) {
            if (t.left() == null && t.right() == null) return true;
            if (t.left() == null && t.right() != null) {
                if (t.read() < t.right().read()) return isABR(t.right());
            } else if (t.left() != null && t.right() == null) {
                if (t.read() > t.left().read()) return isABR(t.left);
            } else {
                if (t.left().read() < t.read() && t.read() < t.right().read())
                    return (isABR(t.left()) && isABR(t.right()));
            }
        }
        return false;
    }

    // Esercizio 6.4
    public static void stampaNodi(BinaryTree<Integer> T, Integer A, Integer B) {
        if (T != null) {
            Integer v = T.read();
            if (v > A) stampaNodi(T.left(), A, B);
            if (A <= v && v <= B) System.out.println(v);
            if (v < B) stampaNodi(T.right(), A, B);
        }
    }

    // Esercizio 6.5
    public static void invisitaIterativa(HashTree<Integer> T) {
        HashTree<Integer> u = T.min(T);
        while (u != null) {
            System.out.println(u.value);
            u = T.successorNode(u);
        }
    }

    // Esercizio 6.6
    public static HashTree<Integer> concat(HashTree<Integer> T1, HashTree<Integer> T2) {
        HashTree<Integer> u = T2.min(T2);
        T2.link(u, T1, T1.key);
        return T2;
    }


    /* Capitolo 9 */

    // Esercizio 9.2
    public void dfs_iterativa(Graph G, int r) {
        Queue<Integer> Q = new LinkedQueue<Integer>();
        boolean[] visitato = new boolean[G.n];
        for (int u : G.V()) visitato[u] = false;
        Q.enqueue(r);
        visitato[r] = true;
        while (!Q.isEmpty()) {
            int u = Q.dequeue();
            for (int v : G.adj(u)) {
                if (!visitato[v]) {
                    Q.enqueue(v);
                    visitato[v] = true;
                }
            }
        }
    }

    // Esercizio 9.5
    public int width(Graph G, int r) {
        int[] p = new int[G.n];
        boolean[] visitato = new boolean[G.n];
        for (int u : G.V()) {
            p[u] = 0;
            visitato[u] = false;
        }
        Queue<Integer> S = new LinkedQueue<Integer>();
        S.enqueue(r);
        visitato[r] = true;
        while (!S.isEmpty()) {
            int u = S.dequeue();
            for (int v : G.adj(u)) {
                if (!visitato[v]) {
                    p[v] = u;
                    visitato[v] = true;
                    S.enqueue(v);
                }
            }
        }
        int[] w = new int[G.n];
        for (int u : G.V()) w[u] = 0;
        for (int u : G.V()) w[u] = w[p[u]] + 1;
        int max = 0;
        for (int i = 0; i < G.n; i++)
            if (w[i] > max)
                max = w[i];
        return max;
    }


    /* Capitolo 11 */

    // Esercizio 11.2
    protected int infinite = Integer.MAX_VALUE;
    public boolean cicloNegativo(Graph G, int r, Integer[] T) {
        int[] d = new int[G.n];  // Distanza di ogni nodo da r
        boolean[] b = new boolean[G.n];
        for (int u : G.V()) {
            T[u] = null;
            d[u] = infinite;
            b[u] = false;
        }
        T[r] = null;
        d[r] = 0;
        b[r] = true;
        LinkedQueue<Integer> S = new LinkedQueue<Integer>();
        S.enqueue(r);
        while (!S.isEmpty()) {
            int u = S.dequeue();
            b[u] = false;
            for (int v : G.adj(u)) {
                if (d[u] + G.w(u, v) < d[v]) {
                    if (!b[v]) {
                        S.enqueue(v);
                        b[v] = true;
                    }
                    T[v] = u;
                    d[v] = d[u] + G.w(u, v);
                }
            }
        }
        // Fin qui abbiamo eseguito n-1 cicli, ora eseguiamo un altro ciclo;
        // se le distanze cambiano allora abbiamo un ciclo negativo.
        for (int u : G.V()) {
            for (int v : G.adj(u)) {
                if (d[u] + G.w(u, v) < d[v])
                    return true;
            }
        }
        return false;
    }


    // Esercizio 11.7
    public int numeroCamminiMinimi(Graph G, int s, int t) {
        int[] d = new int[G.n];
        boolean[] b = new boolean[G.n];
        int[] c = new int[G.n];  // Contatore dei cammini minimi per ogni nodo
        for (int u : G.V()) {
            d[u] = infinite;
            b[u] = false;
            c[u]  = 0;
        }
        d[s] = 0;
        b[s] = true;
        c[s] = 0;
        PriorityQueue<Integer> S = new PriorityQueue<Integer>(G.n);
        S.insert(s, 0);
        while (!S.isEmpty()) {
            int u = S.deleteMin();
            b[u] = false;
            for (int v : G.adj(u)) {
                if (d[u] + G.w(u, v) < d[v]) {
                    if (!b[v]) {
                        S.insert(v, d[u] + G.w(u, v));
                        d[v] = d[u] + G.w(u, v);
                    } else {
                        if (d[v] == d[u] + G.w(u, v)) {
                            c[v] = c[v] + c[u];
                        } else if (d[v] > d[u] + G.w(u, v)) {
                            d[v] = d[u] + G.w(u, v);
                            c[v] = c[u];
                        }
                    }
                }
            }
        }
        return c[t];
    }


    /* Capitolo 12 */

    // Esercizio 12.1
    public static void MergeSort(List<Integer> L, List<Integer>.Pos primo, List<Integer>.Pos ultimo) {
        if (primo != ultimo) {
            int n = 1;
            List<Integer>.Pos mezzo = primo;
            while (mezzo != L.next(ultimo)) {
                n = n + 1;
                mezzo = L.next(mezzo);
            }
            mezzo = primo;
            n = n / 2;
            while (n > 1) {
                mezzo = L.next(mezzo);
                n = n - 1;
            }
            MergeSort(L, primo, mezzo);
            MergeSort(L, L.next(mezzo), ultimo);
            Merge(L, primo, ultimo, mezzo);
        }
    }
    protected static void Merge(List<Integer> L, List<Integer>.Pos primo,
                                List<Integer>.Pos ultimo, List<Integer>.Pos mezzo) {
        mezzo = L.next(mezzo);
        List<Integer>.Pos p = primo, u = ultimo, m = mezzo;
        while (p != mezzo && m != L.next(ultimo)) {
            if (L.read(p) < L.read(m)){
                p = L.next(p);
            } else {
                int t = L.read(p);
                L.write(p, L.read(m));
                L.write(m, t);
                m = L.next(m);
            }
        }
    }

    // Esercizio 12.2
    public static void QuickSort(List<Integer> L, List<Integer>.Pos primo,
                                 List<Integer>.Pos ultimo) {
        if (primo != null && ultimo != null && primo != ultimo && primo != L.next(ultimo)) {
            List<Integer>.Pos p = perno(L, primo, ultimo);
            QuickSort(L, primo, L.prev(p));
            QuickSort(L, L.next(p), ultimo);
        }
    }
    protected static List<Integer>.Pos perno(List<Integer> L, List<Integer>.Pos primo,
                                             List<Integer>.Pos ultimo) {
        int x = L.read(primo);
        List<Integer>.Pos p = primo, q = primo;
        while (p != L.next(ultimo)) {
            if (L.read(p) < x) {
                int tmp = L.read(p);
                q = L.next(q);
                L.write(p, L.read(q));
                L.write(q, tmp);
            }
            p = L.next(p);
        }
        L.write(primo, L.read(q));
        L.write(q, x);
        return q;
    }

    // Esercizio 12.3
    public static int Inversioni(int[] A, int primo, int ultimo) {
        if (primo >= ultimo) {
            return 0;
        } else {
            int mezzo = (primo + ultimo) / 2;
            return (Inversioni(A, primo, mezzo) + Inversioni(A, mezzo + 1, ultimo) +
                    Contamerge(A, primo, ultimo, mezzo));
        }
    }
    protected static int Contamerge(int[] A, int primo, int ultimo, int mezzo) {
        int[] B = new int[ultimo + 1];
        int i = primo, j = mezzo + 1, k = primo, conta = 0;
        while (i <= mezzo && j <= ultimo) {
            if (A[i] <= A[j]) {
                B[k] = A[i];
                i = i + 1;
            }
            else {
                B[k] = A[j];
                j = j + 1;
                conta = conta + mezzo - i + 1;
            }
            k = k + 1;
        }
        while (i <= mezzo) {
            B[k] = A[i];
            i = i + 1;
            k = k + 1;
        }
        while (j <= ultimo) {
            B[k] = A[j];
            j = j + 1;
            k = k + 1;
        }
        for (k = primo; k <= ultimo; k++) A[k] = B[k];
        return conta;
    }

    // Esercizio 12.5
    public static void MergeSortIterativa(int[] A, int n) {
        int[] B = new int[n];
        for (int k = 1; k < n; k *= 2 ) {
            for (int left = 0; left + k < n; left += k * 2 ) {
                int right = left + k;
                int last = right + k;
                if (last > n) last = n;
                int m = left, i = left, j = right;
                while (i < right && j < last) {
                    if (A[i] <= A[j]) {
                        B[m] = A[i];
                        i = i + 1;
                    } else {
                        B[m] = A[j];
                        j = j + 1;
                    }
                    m = m + 1;
                }
                while (i < right) {
                    B[m] = A[i];
                    i= i + 1;
                    m = m + 1;
                }
                while (j < last) {
                    B[m]=A[j];
                    j++; m++;
                }
                for (m = left; m < last; m++) A[m] = B[m];
            }
        }
    }

    // Esercizio 12.9
    public static int conta0(int[] V, int i, int j) {
        // Costo: O(log(n))
        if (i == j || i == j - 1) {
            while (V[i] != 1 && i < j) i = i + 1;
            return i;
        } else {
            int m = (i + j) / 2;
            if (V[m] == 1)
                return conta0(V, i, m);
            else
                return conta0(V, m, j);
        }
    }
    public static int trovaH(int[] V, int n) {
        // Costo: O(log(h))
        int i = 1;
        while (V[i] == 0 && 2 * i < n)
            i = 2 * i;
        if (V[i] == 0)
            return conta0(V, i, n) - 1;
        else if (i / 2 > 1)
            return conta0(V, i / 2, i) - 1;
        else
            return conta0(V, 1, i) - 1;
    }

    // Esercizio 12.11
    public static int ricerca(int[] A, int a, int b) {
        if (a == b) {
            if (A[a] == a)
                return a + 1;
            else
                return a;
        } else {
            int m = (a + b) / 2;
            if (A[m] == m)
                return ricerca(A, m + 1, b);
            else
                return ricerca(A, a, m);
        }
    }

    // Esercizio 12.12
    public static int maxUnimodale(int[] A, int i, int j) {
        int m = (i + j) / 2;
        if (i == j) {
            return A[m];
        } else {
            if (A[m] < A[m + 1])
                return maxUnimodale(A, m + 1, j);
            else
                return maxUnimodale(A, i, m);
        }
    }
    public static int myMaxUnimodale(int[] A, int primo, int ultimo) {
        if (primo <= ultimo) {
            if (primo == ultimo)
                return A[primo];
            int m = (primo + ultimo) / 2;
            if (A[primo] < A[m])
                return myMaxUnimodale(A, m, ultimo);
            else
                return myMaxUnimodale(A, primo, m);
        } else return -1;
    }

    // Esercizio 12.13
    public static int occorrenze(int[] A, int k, int n) {
        int a = binarySearchFirst(A, k, 0, A.length);
        int b = binarySearchLast(A, k, 0, A.length);
        return (b - a + 1);
    }
    protected static int binarySearchFirst(int[] A, int v, int i, int j) {
        if (i > j) {
            return -1;
        } else {
            int m = (i + j) / 2;
            if (v > A[m-1] && v == A[m])
                return m;
            else if (A[m] < v)
                return binarySearchFirst(A, v, m+1, j);
            else
                return binarySearchFirst(A, v, i, m-1);
        }
    }
    protected static int binarySearchLast(int[] A, int v, int i, int j) {
        if (i > j) {
            return -1;
        } else {
            int m = (i + j) / 2;
            if (v < A[m+1] && v == A[m])
                return m;
            else if (A[m] > v)
                return binarySearchLast(A, v, i, m-1);
            else
                return binarySearchLast(A, v, m+1, j);
        }
    }


    /* Capitolo 13 */

    // Esercizio 13.6
    public static Sequence<Integer> maxSeqCrescente(int[] L, int n) {
        int[] LIS = new int[n];
        int[] P = new int[n];
        for (int i = 0; i < n; i++) P[i] = -1;
        for (int i = 0; i < n; i++) {
            int max = 1;
            for (int j = 0; j < i; j++) {
                if (L[j] < L[i] && max < LIS[j] + 1) {
                    max = LIS[j] + 1;
                    P[i] = j;
                }
            }
            LIS[i] = max;
        }
        int LIS_max = posMax(LIS, n);
        Sequence<Integer> S = new Sequence<Integer>();
        while (LIS_max != -1) {
            S.insert(S.head(), L[LIS_max]);
            LIS_max = P[LIS_max];
        }
        return S;
    }
    // Ritorna la posizione dell'elemento di valore massimo nell'array
    private static int posMax(int[] A, int n) {
        int max = A[0];
        int pos = 0;
        for (int i = 1; i < n; i++) {
            if (A[i] > max) {
                max = A[i];
                pos = i;
            }
        }
        return pos;
    }
}
