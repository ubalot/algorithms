package priorityqueue;

// Coda con priorita' decrescente
// Implementazione con limitazione superiore degli elementi a n
public class PriorityQueue<T> {
    protected int capacity;  // Numero massimo degli elementi nella coda
    protected int dim;  // Numero attuale degli elementi nella coda
    protected PriorityItem[]  H;  // Vettore heap : PriorityItem[] H;

    public class PriorityItem {
        public int priority;  // Priorita'
        public T value;  // Elemento
        public int pos;  // Posizione nel vettore heap (serve per la decrease())

        public PriorityItem() {
            this.priority = 0;
            this.value = null;
            this.pos = 0;
        }
    }

    @SuppressWarnings("unchecked")
    public PriorityQueue(int n) {
        this.capacity = n;
        this.dim = 0;
        this.H = (PriorityQueue<T>.PriorityItem[]) new Object[n];
    }

    public boolean isEmpty() {
        return (this.dim == 0);
    }

    // Restituisce elemento minimo di una coda con priorita' non vuota
    public T min() {
        if (this.dim < 0) return null;  // precondition
        return (H[0]).value;  // return H[1].valore
    }

    // Rimuove e restituisce il minimo da una coda con priorita' non vuota
    // Costo: O(h) con h = log(n)
    public T deleteMin() {
        if (dim < 0) return null;
        swap(H, 0, dim);
        dim = dim - 1;
        minHeapRestore(H, 0, dim);
        return H[dim].value;
    }
    protected void minHeapRestore(PriorityItem[] A, int i, int dim) {
        int min = i;
        if (l(i) < dim && A[l(i)].priority < A[min].priority) min = l(i);
        if (r(i) < dim && A[r(i)].priority < A[min].priority) min = r(i);
        if (i != min) {
            swap(A, i, min);
            minHeapRestore(A, min, dim);
        }
    }

    // Inserisce x con priorita' p in una coda con priorita' non piena
    // Restituisce un intero che identifica x all'interno della coda (serve per la decrease())
    public PriorityItem insert(T x, int p) {
        if (this.dim >= this.capacity) return null;  // precondition
        dim = dim + 1;
        H[dim] = new PriorityItem();
        H[dim].value = x;
        H[dim].priority = p;
        H[dim].pos = dim;
        int i = dim;
        while (i > 0 && H[i].priority < H[i].priority) {
            swap(H, i, p(i));
            i = p(i);
        }
        return H[i];
    }
    private void swap(PriorityItem[] h, int i, int j) {
        PriorityItem tmp = h[i];
        h[i] = h[j];
        h[j] = tmp;
        h[i].pos = i;
        h[j].pos = j;
    }


    public void decrease(T X, int p) {
        PriorityItem x = new PriorityItem();
        x.value = X;
        if (p >= x.priority) return ;
        x.priority = p;
        int i = x.pos;
        while (i > 0 && H[i].priority < H[p(i)].priority) {
            swap(H, i, p(i));
            i = p(i);
        }
    }

    // i                                       : posizione elemento i
    // p(i) = i / 2 (arrotondamento inferiore) : posizione padre dell'elemento
    // l(i) = 2 * i                            : posizione figlio sinistro
    // r(i) = 2 * i + 1                        : posizione figlio destro
    private int p(int i) { return (i / 2); }
    private int l(int i) { return (2 * i); }
    private int r(int i) { return ((2 * i) + 1); }
}
