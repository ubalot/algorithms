package queue;

public class ArrayQueue<T> implements Queue<T>{
    private T[] A;
    private int n;
    private int head;
    private int m;

    @SuppressWarnings("unchecked")
    public ArrayQueue(int dim){
        this.A = (T[]) new Object[dim];
        this.m = dim;
        this.head = 0;
        this.n = 0;
    }

    public T top(){
        if(this.n <= 0) return null;  //precondition
        return this.A[this.head];
    }

    public boolean isEmpty(){
        return this.n == 0;
    }

    public T dequeue(){
        if(this.n <= 0) return null;  //precondition
        T tmp = this.A[this.head];
        this.head = this.head++ % this.n;
        this.n--;
        return tmp;
    }

    public void enqueue(T value){
        if(this.n >= this.m) return ;  //precondition
        this.A[(this.head + this.n) % this.m] = value;
        this.n++;
    }

    public int size() {
        return this.n;
    }
}
