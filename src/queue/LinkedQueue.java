package queue;

import java.util.LinkedList;

public class LinkedQueue<T> extends LinkedList<T> implements Queue<T> {
    protected LinkedList<T> L;

    public LinkedQueue() {
        L = new LinkedList<T>();
    }

    public void enqueue(T o) {
        L.add(o);
    }

    public T dequeue() {
        return L.remove();
    }

    public T top() {
        return this.dequeue();
    }

    public boolean isEmpty() {
        return L.isEmpty();
    }
}