package queue;

public interface Queue<T> {
    public boolean isEmpty();
    public void enqueue(T obj);
    public T dequeue();
    public T top();
}
