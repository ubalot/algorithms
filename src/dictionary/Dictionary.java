package dictionary;

public interface Dictionary {
    public int lookup(int k);
    public void insert(int k, int v);
    public void remove(int k);

}
