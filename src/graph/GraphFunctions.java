package graph;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import dequeue.Dequeue;
import mfset.MFSet;
import priorityqueue.PriorityQueue;
import queue.LinkedQueue;
import set.ListSet;

public class GraphFunctions {
    protected int infinite = Integer.MAX_VALUE;

    // Generica implementazione della visita di un grafo
//	public void visita(Graph G, int r) {
//		Queue<Integer> S = new LinkedList<Integer>();
//		S.add(r);
//		// marca il nodo come gia' scoperto
//		while (S.isEmpty()) {
//			int u = S.remove();
//			// esamina nodo u
//			for (Integer v : G.adj(u)) {
//				// esamina arco (u, v)
//				if (/* v non e' ancora stato scoperto*/) {
//					// marca il nodo v come gia' scoperto
//					S.add(v);
//				}
//			}
//		}
//	}

    // Visita in ampiezza (visita a ventaglio)
    // BFS = Breadth-First-Search
    public void bfs(Graph G, int r) {
        //Queue<Integer> S = new LinkedList<Integer>();
        Queue<Integer> S = new LinkedQueue<Integer>();
        S.add(r);
        boolean[] visitato = new boolean[G.n];
        for (Integer u : G.V()) visitato[u] = false;
        visitato[r] = true;
        while (!S.isEmpty()) {
            Integer u = S.remove();
            // esamina nodo u
            for (Integer v : G.adj(u)) {
                // esamina l'arco (u, v)
                if (!visitato[v]) {
                    visitato[v] = true;
                    S.add(v);
                }
            }
        }
    }

    // Visita in profondita' (visita a scandaglio)
    // DFS = Depth-First-Search
    public void dfs(Graph G, int u, boolean[] visitato) {
        visitato[u] = true;
        // esamina il nodo u (previsita)
        for (int v : G.adj(u)) {
            // esamina arco (u, v)
            if (!visitato[v])
                dfs(G, v, visitato);
        }
        // esamina il nodo u (postvisita)
    }

    // un grafo non orientato e' connesso? (tutti i suoi nodi sono collegati tra loro)
    // Costo: O(n + m)
    public boolean nonOrientedGraphIsConnected(Graph G, int u) {
        boolean[] visitato = new boolean[G.n];
        for (int i = 0; i < visitato.length; i++) visitato[i] = false;
        dfs(G, u, visitato);
        for (int i = 0; i < visitato.length; i++) {
            if (!visitato[i])
                return false;
        }
        return true;
    }

    // componenti connesse di un grafo non orientato
    public int[] cc(Graph G, int[] ordine) {
        // ordine[] e' una permutazione dei nodi di G
        int[] id = new int[G.n];  // per ogni nodo identifica la componente connessa a cui esso appartiene
        for (int u : G.V()) id[u] = 0;
        int conta = 0;
        for (int i = 0; i < G.n; i++) {
            if (id[ordine[i]] == 0) {
                conta = conta + 1;
                ccdfs(G, conta, ordine[i], id);
            }
        }
        return id;
    }
    private void ccdfs(Graph G, int conta, int u, int[] id) {
        id[u] = conta;
        for (int v : G.adj(u)) {
            if (id[v] == 0) {
                ccdfs(G, conta, v, id);
            }
        }
    }

    int time = 0; // System.currentTimeMills()
    int[] dt, ft;
    // dfs-schema: schema per varie visite dfs
    public void dfs_schema(Graph G, int u) {
        // esamina il nodo u (pre-visita)
        time = time + 1;
        dt[u] = time;
        for (int v : G.adj(u)) {
            // esamina arco (u, v) di qualsiasi tipo
            if (dt[v] == 0) {
                // esamina arco (u, v) in T
                dfs_schema(G, v);
            } else if (dt[u] > dt[v] && ft[v] == 0) {
                // esamina arco (u, v) all'indietro
            } else if (dt[u] < dt[v] && ft[v] != 0) {
                // esamina arco (u, v) in avanti
            } else {
                // esamina arco (u, v) di attraversamento
            }
        }
        // esamina il nodo u (post-visita)
        time = time + 1;
        ft[u] = time;
    }

    // funzione che restuituisce true se il grafo e' ciclico (contine almeno un ciclo),
    // restituisce false se il grafo e' aciclico
    // (variante del dfs-schema)
    public boolean ciclico(Graph G, int u) {
        time = time + 1;
        dt[u] = time;
        for (int v : G.adj(u)) {
            if (dt[v] == v) {
                if (ciclico(G, v)) return true;
            } else if (dt[u] > dt[v] && ft[v] == 0) {
                return false;
            }
        }
        time = time + 1;
        ft[u] = time;
        return false;
    }

    // procedura equivalente a cc() ma efficace per i grafi orientati
    public int[] scc(Graph G) {
        Stack<Integer> S = new Stack<Integer>();		// Prima visita
        boolean[] visitato = new boolean[G.n];
        for (int u : G.V()) visitato[u] = false;
        for (int u : G.V()) dfsStack(G, visitato, S, u);

        Graph Gt = new Graph(G.n);						// Calcolo grafo trasposto
        for (int u : G.V()) Gt.insertNode(u);
        for (int u : G.V()) {
            for (int v : G.V()) {
                Gt.insertEdge(v, u, 1);
            }
        }

        int[] ordine = new int[G.n];					// Seconda visita
        for (int i = 0; i < G.n; i++) ordine[i] = S.pop();
        return cc(Gt, ordine);
    }
    protected void dfsStack(Graph G, boolean[] visitato, Stack<Integer> S, int u) {
        visitato[u] = true;
        for (int v : G.adj(u)) {
            if (!visitato[v]) {
                dfsStack(G, visitato, S, v);
            }
        }
        S.push(u);
    }

    public void topSort(Graph G, int[] ordine) {
        boolean[] visitato = new boolean[G.n];
        for (int u : G.V()) visitato[u] = false;
        int i = 0;
        for (int u : G.V()) {
            if (!visitato[u]) {
                i =  ts_dfs(G, u, i, visitato, ordine);
            }
        }
    }
    protected int ts_dfs(Graph G, int u, int i, boolean[] visitato, int[] ordine) {
        visitato[u] = true;
        for (int v : G.adj(u)) {
            if (!visitato[v]) {
                i = ts_dfs(G, v, i , visitato, ordine);
            }
        }
        ordine[G.n - i] = u;
        return i + 1;
    }

    // Algoritmo cammini minimi di Dijkstra
    // Costo: O(n^2)
    // E' efficiente per grafi densi con archi non negativi.
    // T e' il vettore dei padri, al termine dell'esecuzione conterra' l'albero dei
    // cammini minimi.
    public void camminiMinimiDijkstra(Graph G, int r, Integer[] T) {
        int[] d = new int[G.n];  // Distanza di ogni nodo da r
        boolean[] b = new boolean[G.n];
        for (int u : G.V()) {
            T[u] = null;
            d[u] = infinite;
            b[u] = false;
        }
        T[r] = null;
        d[r] = 0;
        b[r] = true;
        PriorityQueue<Integer> S = new PriorityQueue<Integer>(G.n);
        S.insert(r, 0);
        while (!S.isEmpty()) {
            int u = S.deleteMin();
            b[u] = false;
            for (int v : G.adj(u)) {
                if (d[u] + G.w(u, v) < d[v]) {
                    if (!b[v]) {
                        S.insert(v, d[u] + G.w(u, v));
                        b[v] = true;
                    } else {
                        S.decrease(v, d[u] + G.w(u, v));
                    }
                    T[v] = u;
                    d[v] = d[u] + G.w(u, v);
                }
            }
        }
    }

    // L'algoritmo di Johnson e' simile a quello di Dijkstra ma usa gli heap binari.
    // Costo: O(m * logn(n))
    // Per grafi sparsi la complessita' diventa O(n * log(n))

    // Algoritmo cammini minimi di Bellman-Ford-Moore
    // Costo: O(n * m)
    // L'algoritmo e' polinomiale anche per grafi con archi negativi.
    public void camminiMinimiBFM(Graph G, int r, Integer[] T) {
        int[] d = new int[G.n];  // Distanza di ogni nodo da r
        boolean[] b = new boolean[G.n];
        for (int u : G.V()) {
            T[u] = null;
            d[u] = infinite;
            b[u] = false;
        }
        T[r] = null;
        d[r] = 0;
        b[r] = true;
        LinkedQueue<Integer> S = new LinkedQueue<Integer>();
        S.enqueue(r);
        while (!S.isEmpty()) {
            int u = S.dequeue();
            b[u] = false;
            for (int v : G.adj(u)) {
                if (d[u] + G.w(u, v) < d[v]) {
                    if (!b[v]) {
                        S.enqueue(v);
                        b[v] = true;
                    }
                    T[v] = u;
                    d[v] = d[u] + G.w(u, v);
                }
            }
        }
    }

    // Algoritmo cammini minimi di Pape-D'Esopo
    // L'algoritmo ha costo superpolinomiale nel caso pessimo, ma questo non si verifica mai.
    // E' l'algoritmo piu' efficiente, soprattuto per grafi sparsi e planari (gli archi non
    // si incrociano).
    public void camminiMinimiPE(Graph G, int r, Integer[] T) {
        int[] d = new int[G.n];  // Distanza di ogni nodo da r
        boolean[] b = new boolean[G.n];
        for (int u : G.V()) {
            T[u] = null;
            d[u] = infinite;
            b[u] = false;
        }
        T[r] = null;
        d[r] = 0;
        b[r] = true;
        Dequeue<Integer> S = new Dequeue<Integer>();
        S.insertHead(r);
        while (!S.isEmpty()) {
            int u = S.removeHead();
            b[u] = false;
            for (int v : G.adj(u)) {
                if (d[u] + G.w(u, v) < d[v]) {
                    if (!b[v]) {
                        if (d[v] == infinite)
                            S.insertTail(v);
                        else
                            S.insertHead(v);
                        b[v] = true;
                    }
                    T[v] = u;
                    d[v] = d[u] + G.w(u, v);
                }
            }
        }
    }

    // L'algoritmo trova il minimo albero di copertura (albero avente tutti i nodi di V,
    // ma solo n-1 degli m archi in E) dato un grafo non orientato e connesso, con pesi
    // non negativi sugli archi.
    // Costo: O(m * log(n))
    public class Arco {
        int u;
        int v;
        int peso;
    }
    public ListSet<Arco> kruskal(Arco[] A, int n, int m) {
        ListSet<Arco> T = new ListSet<Arco>();
        MFSet M = new MFSet(n);
        ordinaArco(A);
        int c = 0;
        int i = 1;
        while (c < n - 1 && i <= m) {
            if ( M.find(A[i].u) != M.find(A[i].v) ) {
                M.merge(A[i].u, A[i].v);
                T.insert(A[i]);
                c = c + 1;
            }
            i = i + 1;
        }
        return T;
    }
    // Ordina A in modo che A[0].peso <= A[1].peso <= ... <= A[n-1].peso
    private void ordinaArco(Arco[] A) {
        for (int i = 1; i < A.length; i++) {
            int temp = A[i].peso;
            int j = i;
            while (j > 0 && A[j-1].peso > temp) {
                A[j] = A[j-1];
                j--;
            }
            A[j].peso = temp;
        }
    }

    // Algoritmo per trovare il minimo albero di copertura di un grafo.
    // Costo: O(m * log(n))
    // Buono per grafi sparsi (m = O(n))
    // Per grafi densi conviene usare una lista al posto della coda con priorita', costo: O(n^2).
    public void prim(Graph G, int r, int[] p) {
        // p e' il vettore dei padri
        PriorityQueue<Integer> Q = new PriorityQueue<Integer>(G.n);  // coda con priorita' minima
        @SuppressWarnings("unchecked")
        PriorityQueue<Integer>.PriorityItem[] pos = (PriorityQueue<Integer>.PriorityItem[]) new Object[G.n];
        for (int u : G.V()) pos[u] = Q.insert(u, infinite);
        pos[r] = Q.insert(r, 0);
        p[r] = 0;
        while (!Q.isEmpty()) {
            int u = Q.deleteMin();
            pos[u] = null;
            for (int v : G.adj(u)) {
                if (pos[v] != null && G.w(u, v) < pos[v].priority) {
                    Q.decrease(pos[v].value, G.w(u, v));
                    p[v] = u;
                }
            }
        }
    }
}
