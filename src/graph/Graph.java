package graph;

import java.util.Set;
import java.util.TreeSet;

public class Graph {
    protected int[] V;
    protected int[][] M;  // Matrice di adiacenza nodi-nodi
    public int n;  // Numero nodi del grafo

    public Graph(int d) {
        int dim = d + 1;
        V = new int[dim];
        M = new int[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                M[i][j] = 0;
            }
        }
        n = 0;
    }

    public void insertNode(int u) {
        V[u] = 1;
        n++;
    }

    public void insertEdge(int u, int v, int w) {
        M[u][v] = w;
        M[v][u] = w;
    }

    public void deleteNode(Integer u) {
        V[u] = 0;
        n--;
    }

    public void deleteEdge(int u, int v) {
        M[u][v] = 0;
        M[v][u] = 0;
    }

    // Restituisce insieme dei nodi adiacenti ad u
    public Set<Integer> adj(int u) {
        Set<Integer> S = new TreeSet<Integer>();
        for (int i = 0; i < M[u].length; i++) {
            if (M[u][i] == 1)
                S.add(i);
        }
        return S;
    }

    // Restituisce l'insieme di tutti i nodi
    public Set<Integer> V() {
        Set<Integer> S = new TreeSet<Integer>();
        for (int i = 0; i < V.length; i++) {
            if (V[i] == 1)
                S.add(i);
        }
        return S;
    }

    public int w(int u, int v) {
        return M[u][v];
    }
}
