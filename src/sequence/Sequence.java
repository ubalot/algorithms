package sequence;

// lista bilinkata con due sentinelle
public class Sequence<T> {
    protected Pos head;  // head sentinel
    protected Pos tail;  // tail sentinel

    public class Pos {
        protected T val;
        protected Pos prev;
        protected Pos next;

        public Pos(T v){
            this.val = v;
            this.prev = null;
            this.next = null;
        }
    }

    public Sequence(){
        this.head = new Pos(null);
        this.tail = new Pos(null);
        this.head.prev = null;
        this.head.next = this.tail;
        this.tail.prev = this.head;
        this.tail.next = null;
    }

    public boolean empty(){
        return this.head.next == this.tail ||
                this.tail.prev == this.head;
    }

    public boolean finished(Pos p){
        return p == this.tail;
    }

    // returns element after head sentinel
    public Pos head(){
        return this.head.next;
    }

    // returns last element before tail sentinel
    public Pos tail(){
        return this.tail.prev;
    }

    public Pos next(Pos p){
        return p.next;
    }

    public Pos prev(Pos p){
        return p.prev;
    }

    public Pos insert(Pos p, T v){
        Pos temp = new Pos(v);
        temp.prev = p.prev;
        temp.prev.next = temp;
        temp.next = p;
        p.prev = temp;
        return temp;
    }

    public Pos remove(Pos p){
        p.prev.next = p.next;
        p.next.prev = p.prev;
        return p;
    }

    public int read(Pos p){
        return (p.val != null ? Integer.parseInt(p.val.toString()) : 0);
    }

    public void write(Pos p, T v){
        p.val = v;
    }

    @Override
    public String toString(){
        String result = "";
        Pos tmp = this.head();
        while (!this.finished(tmp)) {
            result += (tmp.val != null ? tmp.val.toString() : "") + " ";
            tmp = this.next(tmp);
        }
        return result;
    }

}
