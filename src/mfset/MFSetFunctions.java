package mfset;

import graph.Graph;

public class MFSetFunctions {

    // Identifica componenti connesse di un grafo
    public MFSet cc(Graph G) {
        MFSet M = new MFSet(G.n);
        for (int u : G.V()) {
            for (int v : G.adj(u)) {
                M.merge(u, v);
            }
        }
        return M;
    }
}
