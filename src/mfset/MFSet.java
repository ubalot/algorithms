package mfset;

public class MFSet {
    protected int[] p;
    protected int[] rango;  // Serve per la merge(), euristica del rango
    // rango contiene l'altezza dell'albero radicato nell'i-esimo nodo

    public MFSet(int n) {
        p = new int[n + 1];
        rango = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            p[i] = i;
            rango[i] = 0;
        }
    }

    // Implementazione inefficiente
//	public int find(int x) {
//		if (p[x] == x)
//			return x;
//		else
//			return find(p[x]);
//	}
    // Ogni volta che la find() viene eseguita modifica l'albero ponendo ogni nodo che incontra
    // come figlio della radice (compressione dei percorsi).
    public int find(int x) {
        if (p[x] != x)
            p[x] = find(p[x]);
        return p[x];
    }

    // Questa implementazione puo' essere inefficiente se si produce un albero di altezza massima.
//	public void merge(int x, int y) {
//		int r_x = find(x);
//		int r_y = find(y);
//		if (r_x != r_y)
//			p[r_y] =r_x;
//	}
    // Implementazione basata su euristica del rango:
    // si unisce l'albero di altezza inferiore a quello di altezza maggiore (si attacca alla radice)
    // nel tentativo di ridurre l'altezza dell'albero finale.
    public void merge(int x, int y) {
        int r_x = find(x);
        int r_y = find(y);
        if (r_x != r_y) {
            if (rango[r_x] > rango[r_y]) {
                p[r_y] = r_x;
            } else if (rango[r_y] > rango[r_x]) {
                p[r_x] = r_y;
            } else {
                p[r_x] = r_y;
                rango[r_y] = rango[r_y] + 1;
            }
        }

    }
}
