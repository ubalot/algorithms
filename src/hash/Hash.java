package hash;

public class Hash {
    protected Integer[] A;  // Tabella delle chiavi
    protected String[] V;  // Tabella dei valori
    protected int m;  // Dimensione della tabella

    public Hash(int capacity) {
        m = capacity;
        A = new Integer[m - 1];
        V = new String[m - 1];
        for (int i = 0; i < m - 1; i++) A[i] = null;
    }

    // 'deleted' viene rappresentato con -1
    public int scan(Integer k, boolean insert) {
        int c = m;  // Prima posizione 'deleted'
        int i = 0;  // Numero di ispezione
        int j = H(k.toString().toCharArray());  // Posizione attuale
        while (A[j] != k && A[j] != null && i < m) {
            if (A[j] == -1 && c == m) c = j;  // Prima posizione cancellata
            j = (j + H1(k.toString().toCharArray())) % m;
            i = i + 1;
        }
        if (insert && A[j] != k && c < m) j = c;  // Prima posizione cancellata
        return j;
    }

    public String lookup(Integer k) {
        int i = scan(k, false);
        if (A[i] == k) {
            return V[i];
        } else {
            return null;
        }
    }

    public void insert(Integer k, String v) {
        int i = scan(k, true);
        if (A[i] == null || A[i] == -1 || A[i] == k) {
            A[i] = k;
            V[i] = v;
        } else {
            System.out.println("Errore: tabella hash piena");
        }
    }

    public void remove(Integer k) {
        int i = scan(k, false);
        if (A[i] == k) {
            A[i] = -1;
        }
    }

    protected int H(char[] k) {
        int b = Character.getNumericValue(k[0]);
        int m = 6143; // 13, 23, 47, 97, 193, 383, 769, 1531, 3067, 6143, 12289, 24571
        for (int j = 1; j < k.length; j++) {
            b = ((b * 64) + Character.getNumericValue(k[j])) % m;
        }
        return b;
    }

    protected int H1(char[] k) {
        int b = Character.getNumericValue(k[0]);
        int m = 3067; // 13, 23, 47, 97, 193, 383, 769, 1531, 3067, 6143, 12289, 24571
        for (int j = 1; j < k.length; j++) {
            b = ((b * 64) + Character.getNumericValue(k[j])) % m;
        }
        return b;
    }
}
