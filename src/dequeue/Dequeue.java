package dequeue;

public class Dequeue<T> {
    protected Pos head;  // head sentinel

    public class Pos {
        protected T val;
        protected Pos pred;
        protected Pos succ;

        public Pos(T v) {
            this.val = v;
            pred = succ = null;
        }
    }


    public Dequeue() {
        this.head = new Pos(null);
        this.head.pred = this.head.succ = this.head;
    }

    public boolean isEmpty() {
        return this.head.succ == this.head;
    }

    public Pos head() {
        return this.head.succ;
    }

    public Pos tail() {
        return this.head.pred;
    }

    public Pos next(Pos p) {
        return p.succ;
    }

    public Pos prev(Pos p) {
        return p.pred;
    }

    public boolean finished(Pos p) {
        return p == this.head;
    }

    public int read(Pos p) {
        return (p.val != null ? Integer.parseInt(p.val.toString()) : 0);
    }

    public void write(Pos p, T v) {
        p.val = v;
    }

    // inserisce l'elemento in testa
    public void insertHead(T v) {
        Pos t = new Pos(v);
        t.pred = this.head().pred;
        t.succ = this.head.succ;
        t.succ.pred = t;
    }

    // inserisce l'elemento in coda
    public void insertTail(T v) {
        Pos t = new Pos(v);
        t.pred = this.tail();
        t.succ = t.pred.succ;
        t.pred.succ = t;
    }

    // rimuove l'elemento in testa
    public T removeHead() {
        Pos t = this.head();
        this.head.succ = t.succ;
        t.succ.pred = this.head;
        return t.val;
    }

    @Override
    public String toString() {
        String result = "";
        Pos p = this.head();
        while (!this.finished(p)) {
            result += (p.val != null ? p.val.toString() : "") + " ";
            p = this.next(p);
        }
        return result;
    }

    public T getElem(Pos p) {
        return p.val;
    }
}
