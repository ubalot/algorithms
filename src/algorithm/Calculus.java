package algorithm;

public class Calculus {
    // Chiamare con moltiplicazioneEgizia(a, b, 0)
    // Restituisce a * b
    public static int moltiplicazioneEgizia(int a, int b, int p){
        if(a < 0 || b < 0) return -1;

        if(a == 0) return p;
        if(a % 2 == 1){
            p += b;
        }
        a /= 2;
        b *= 2;
        return moltiplicazioneEgizia(a, b, p);
    }

    // Esempio di funzione hash
    public static int H(char[] k, int l) {
        int b = Character.getNumericValue(k[0]);
        int m = 13; // 13, 23, 47, 97, 193, 383, 769, 1531, 3067, 6143, 12289, 24571
        for (int j = 1; j < l; j++) {
            b = ((b * 64) + Character.getNumericValue(k[j])) % m;
        }
        return b;
    }

    public static int max(int a, int b) {
        return (a > b ? a : b);
    }

    public static int min(int a, int b) {
        return (a < b ? a : b);
    }

    // Costo: O(n^2)
    public void moltiplicaPolinomi(int[] p, int[] q, int[] r, int n) {
        for (int h = 0; h < 2 * (n - 1); h++) r[h] = 0;
        for (int h = 0; h < n - 1; h++)
            for (int k = 0; k < n - 1; k++)
                r[h + k] = r[h + k] + p[h] + q[k];
    }

    // Moltiplica due matrici quadrate (n * n).
    // Costo: O(n^3)
    public void moltiplicaMatrici(int[][] A, int[][] B, int[][] C, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] = 0;
                for (int k = 0; k < n; k++)
                    C[i][j] = C[i][j] + A[i][k] * B[k][j];
            }
        }
    }

    // Esempio programmazione dinamica con vettore
    public static int fibonacci_vet(int n) {
        int[] F = new int[max(1, n+1)];
        F[0] = F[1] = 1;
        for (int i = 2; i <= n; i++)
            F[i] = F[i - 1] + F[i - 2];
        return F[n];
    }
    // Esempio programmazione dinamica senza vettore
    public static int fibonacci(int n) {
        int F0, F1, F2;
        F0 = F1 = F2 = 1;
        for (int i = 2; i <= n; i++) {
            F0 = F1;
            F1 = F2;
            F2 = F0 + F1;
        }
        return F2;
    }
}
