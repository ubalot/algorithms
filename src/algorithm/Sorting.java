package algorithm;

import java.util.Random;

import priorityqueue.*;


public class Sorting {
    // Funzione di ordinamento banale,
    // prova tutte le possibili permutazioni di A e controlla se A e' ordinato
    // Complessita': O(n * n!)
    public static void stupidSort(int[] A, int n) {
        Random generator = new Random();
        while (!isSorted(A)) {
            for (int i = 0; i < n; i++) {
                swap(A, i, generator.nextInt(n));
            }
        }
    }

    // Funzione ausiliaria di stupidSort
    public static boolean isSorted(int[] A) {
        for (int i = 1; i < A.length; i++) {
            if (A[i-1] > A[i]) {
                return false;
            }
        }
        return true;
    }



    // Funzione di ordinamento array
    // Complessita': O(n^2)
    public static void selectionSort(int[] A, int n) {
        for (int i = 0; i < n; i++) {
            int j = min(A, i, n);
            swap(A, i, j);
        }
    }
    // Funzione ausiliaria per selectionSort()
    private static int min(int[] A, int k, int n) {
        int min = k;
        for (int h = k+1; h < n; h++) {
            if (A[h] < A[min]) {
                min = h;
            }
        }
        return min;
    }
    // Funzione ausiliaria per selectionSort()
    public static void swap(int[] A, int pos1, int pos2) {
        int temp = A[pos1];
        A[pos1] = A[pos2];
        A[pos2] = temp;
    }


    // Ordina l'array scorrendolo: ogni elemento viene posto nella parte che lo
    // precede dell'array, secondo l'ordine ascendente
    // Complessita': O(n^2)
    public static void insertionSort(int[] A, int n){
        for (int i = 1; i < n; i++) {
            int temp = A[i];
            int j = i;
            while (j > 0 && A[j-1] > temp) {
                A[j] = A[j-1];
                j--;
            }
            A[j] = temp;
        }
    }

    public static void mergeSort(int[] A, int first, int last){
        mergesort(A, first, last-1);
    }
    // Ordina la prima meta' dell'array, poi la seconda meta', alla fine
    // unisce le due meta' ordinandone gli elementi.
    // Chiamata: mergesort(A, 0, A.length-1)
    // Complessita': O(n * log(n))
    private static void mergesort(int[] A, int first, int last) {
        if (first < last) {
            int middle = (first + last) / 2;
            mergesort(A, first, middle);
            mergesort(A, middle + 1, last);
            merge(A, first, last, middle);
        }
    }
    // Funzione ausiliaria che si occupa di ordinare un array, le cui due meta'
    // sono state preventivamente ordinate
    private static void merge(int[] A, int first, int last, int middle) {
        int[] B = new int[last + 1];
        int i = first, j = middle + 1, k = first;
        while (i <= middle && j <= last) {
            if (A[i] <= A[j]) {
                B[k] = A[i];
                i = i + 1;
            }
            else {
                B[k] = A[j];
                j = j + 1;
            }
            k = k + 1;
        }
        while (i <= middle) {
            B[k] = A[i];
            i = i + 1;
            k = k + 1;
        }
        while (j <= last) {
            B[k] = A[j];
            j = j + 1;
            k = k + 1;
        }
        for (k = first; k <= last; k++) A[k] = B[k];
    }


    // Ordina l'array A usando un array di appoggio B[k] dove k e' il valore
    // massimo presente nell'array: B contiene in ogni cella il conteggio
    // per ogni valore presente in A.
    // Complessita': O(n + k)   se k e' O(n) => countingSort e' meglio di mergeSort
    public static void countingSort(int[] A, int n, int k) {
        int i, j;
        int[] B = new int[k];
        for (i = 0; i < k; i++) B[i] = 0;
        for (j = 0; j < n; j++) B[A[j]] += 1;
        j = 0;
        for (i = 1; i < k; i++) {
            while (B[i] > 0) {
                A[j] = i;
                j++;
                B[i] -= 1;
            }
        }
    }

    // Ordina l'array A creando una coda con priorita' Q ausiliaria all'ordinamento.
    public static void sort(int[] A, int n) {
        PriorityQueue<Integer> Q = new PriorityQueue<Integer>(n + 1);
        for (int i = 0; i < n; i++) Q.insert(A[i], A[i]);
        for (int i = 0; i < n; i++) A[i] = Q.deleteMin();
    }


    // Ordinamento dell'array A tramite heap
    // Costo: O(n * log(n))
    public static void heapsort(int[] A) {
        heapBuild(A, A.length - 1);
        for (int i = A.length - 1; i >= 2; i--) {
            swap(A, i, 1);
            maxHeapRestore(A, 1, i - 1);
        }
    }
    // Ordina l'array A in loco trasformandolo in una coda con priorita' crescente.
    // Costo: O(n)
    public static void heapBuild(int[] A, int n) {
        for (int i = (n / 2); i >= 1; i--)  // da (n/2 +1) fino a n sono tutte foglie, quindi sono gia' degli heap
            maxHeapRestore(A, i, n);
    }
    // Costo: O(log(n))
    private static void maxHeapRestore(int[] A, int i, int dim) {
        int max = i;
        if (l(i) <= dim && A[l(i)] > A[max]) max = l(i);
        if (r(i) <= dim && A[r(i)] > A[max]) max = r(i);
        if (i != max) {
            swap(A, i, max);
            maxHeapRestore(A, max, dim);
        }
    }

    private static int l(int i) { return (2 * i); }
    private static int r(int i) { return ((2 * i) + 1); }


    public static void quickSort(int[] A, int primo, int ultimo) {
        QuickSort(A, primo, ultimo - 1);
    }
    // Cerca un elemento perno e sposta a sinistra tutti gli elementi minori di perno,
    // a destra quelli maggiori.
    // Costo: caso pessimo O(n^2)
    //        caso medio O(n * log(n))
    public static void QuickSort(int[] A, int primo, int ultimo) {
        if (primo < ultimo) {
            int j = perno(A, primo, ultimo);
            QuickSort(A, primo, j -1);
            QuickSort(A, j + 1, ultimo);
        }
    }
    public static int perno(int[] A, int primo, int ultimo) {
        int x = A[primo];
        int j = primo;
        for (int i = primo; i <= ultimo; i++) {
            if (A[i] < x) {
                j = j + 1;
                swap(A, i, j);
            }
        }
        A[primo] = A[j];
        A[j] = x;
        return j;
    }

    // Esempio di algoritmo di ricerca locale
    // Simile a insertionSort(), ma puo' scambiare elementi non adiacenti
    public static void shellSort(int[] A, int n) {
        int h = 1;
        while (h <= n) h = 3 * h + 1;  // h = 1, 4, 13, 40, 121, 364, 1093, ...
        h = h / 3;
        while (h >= 1) {
            for (int i = h; i < n; i++) {
                // Questo blocco e' simile a insertioSort()
                int temp = A[i];
                int j = i;
                while (j >= h && A[j - h] > temp) {
                    A[j] = A[j - h];
                    j = j - h;
                }
                A[j] = temp;
            }
            h = h / 3;
        }
    }
}
