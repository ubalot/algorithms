package algorithm;

import java.util.Random;

public class Search {
    // Ritorna valore minimo dell'array
    // Costo: O(n)
    public static int min(int[] A, int n) {
        int min = A[0];
        for (int i = 1; i < n; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }
        return min;
    }

    // Ritorna valore massimo dell'array
    // Costo: O(n)
    public static int max(int[] A, int n) {
        int max = A[0];
        for (int i = 1; i < n; i++) {
            if (A[i] > max) {
                max = A[i];
            }
        }
        return max;
    }


    // L'array deve essere gia' ordinato.
    // Ritorna la posizione dell'elemento con valore v
    // Costo: O(log(n))
    public static int binarySearch(int[] A, int v, int i, int j) {
        if (i > j) {
            return -1;
        } else {
            int m = (i + j) / 2;
            if (A[m] == v) {
                return m;
            } else if (A[m] < v) {
                return binarySearch(A, v, m + 1, j);
            } else {
                return binarySearch(A, v, i, m - 1);
            }
        }
    }


    // L'array deve essere gia' ordinato.
    // Ritorna la posizione dell'elemento con valore v
    // Costo: O(log(n))
    public static int iterativeBinarySearch(int[] A, int v, int n) {
        int i = 1, j = n, m = (i + j) / 2;
        while (i < j && A[m] != v) {
            if (A[m] < v) {
                i = m + 1;
            } else {
                j = m - 1;
            }
            m = (i + j) / 2;
        }
        if (i > j || A[m] != v) {
            return -1;
        } else {
            return m;
        }
    }

    // Come binarySearch() ma cerca di avvicinarsi quanto piu' possibile al valore
    // cercato al primo tentativo.
    // Costo: O(log(log(n))) nel caso le chiavi siano distribuite uniformemente.
    public static int ricercaInterpolata(int[] A, int k, int i, int j) {
        if (i > j) {
            return -1;
        } else {
            int m = i + ( (k - A[i]) * ( (j - i) / (A[j] - A[i]) ) );
            if (A[m] == k) {
                return m;
            } else if (A[m] < k) {
                return binarySearch(A, k, m + 1, j);
            } else {
                return binarySearch(A, k, i, m - 1);
            }
        }
    }

    // Esempio programmazione dinamica
    // Trova un'occorrenza 0-approssimata di P in T
    public static int stringMatching(char[] P, char[] T, int m, int n) {
        // P = pattern, T = testo, m = P.length, n = T.length
        int[][] D = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++) D[i][0] = i;
        for (int j = 0; j <= n; j++) D[0][j] = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                int t = D[i-1][j-1] + (P[i-1] == T[j-1] ? 0 : 1);
                t = Calculus.min(t, D[i-1][j] + 1);
                t = Calculus.min(t, D[i][j-1] + 1);
                D[i][j] = t;
            }
        }
        int min = D[m][0];
        int pos = 0;
        for (int j = 1; j <= n; j++) {
            if (D[m][j] < min) {
                min = D[m][j];
                pos = j;
            }
        }
        return pos;
    }

    // Ritorna numero minimo di operazioni per passare da una stringa all'altra
    public static int editDistance(char[] P, char[] T, int m, int n) {
        // P = stringa1, T = stringa2, m = P.length, n = T.length
        int[][] D = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++) D[i][0] = i;
        for (int j = 0; j <= n; j++) D[0][j] = j;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                int t = D[i-1][j-1] + (P[i-1] == T[j-1] ? 0 : 1);
                t = Calculus.min(t, D[i-1][j] + 1);
                t = Calculus.min(t, D[i][j-1] + 1);
                D[i][j] = t;
            }
        }
        return D[m][n];
    }

    // Trova il minimo k per l'occorrenza k-approssimata di P in T
    public static int stringMatchingSingleRow(char[] P, char[] T, int m, int n) {
        int[] D = new int[n + 1];
        for (int j = 0; j <= n; j++) D[j] = 0;
        for (int i = 1; i <= m; i++) {
            int dx = 1;  // uguale a D[j - 1]
            for (int j = 1; j <= n; j++) {
                int t = min(D[j-1] + (P[i-1] == T[j-1] ? 0 : 1 ), D[j] + 1, dx + 1);
                dx = D[j];
                D[j] = t;
            }
            D[0] = i;
        }
        int min = D[0];
        for (int j = 1; j <= n; j++)
            min = Calculus.min(min, D[j]);
        return min;
    }
    private static int min(int i, int j, int k) {
        if (i < j && i < k) return i;
        else if (j < i && j < k) return j;
        else return k;
    }

    // Esempio di algoritmo backtrack.
    // Restituisce indice in T da cui parte l'occorrenza di P.
    // Testo: T[0...n-1]
    // Pattern: P[0...m-1]
    // Costo: O(m * n)
    public static int ricercaBruta(char[] P, char[] T, int n, int m) {
        int i, j, k;
        i = j = k = 0;
        while (i < n && j < m) {
            if (T[i] == P[j]) {
                i = i + 1;
                j = j + 1;
            } else {
                k = k + 1;
                i = k;
                j = 0;
            }
        }
        return (j >= m ? k : i);
    }

    // Esempio di algoritmo di backtrack.
    // Miglioramento di ricercaBruta.
    // Costo: O(m + n)
    public static int kmp(char[] T, char[] P, int n, int m) {
        // n = T.length, m = P.length
        int[] back = new int[m];
        computeBack(P, back, m);
        int i = 0;
        int j = 0;
        while (i < n && j < m) {
            if (j == 0 || T[i] == P[j]) {
                i = i + 1;
                j = j + 1;
            } else {
                j = back[j];
            }
        }
        return (j >= m ? i - m : i);
    }
    protected static void computeBack(char[] P, int[] back, int m) {
        back[0] = 0;
        int j = 1;
        int h = 0;
        while (j < m-1) {
            if (h == 0 || P[j] == P[h]) {
                j = j + 1;
                h = h + 1;
                back[j] = (P[j] == P[h] ? back[h] : h);
            } else {
                h = back[h];
            }
        }
    }

    // Seleziona k-esimo piu' piccolo.  NON FUNZIONA
    // Costo: O(n) nel caso medio
    public static int selezione(int[] A, int primo, int ultimo, int k) {
        if (primo == ultimo) {
            return A[primo];
        } else {
            Random random = new Random();
            int randomPos = random.nextInt(ultimo - primo) + primo;
            Sorting.swap(A, randomPos, primo);
            int j = Sorting.perno(A, primo, ultimo);
            int q = j - primo +1;  // Numero elementi di A[primo...j]
            System.out.println("primo " +primo);
            System.out.println("ultimo " +ultimo);
            System.out.println("j " +j);
            System.out.println("q " +q);
            if (k <= q)
                return selezione(A, primo, j, k);
            else
                return selezione(A, j+1, ultimo, k);
        }
    }
}
