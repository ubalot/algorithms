package array;

import java.util.Random;

public class Array {
    public static final int limit = 99;

    public static String toString(int[] A) {
        String result = "[ ";
        for (int i = 0; i < A.length; i++) {
            result += A[i];
            if (i != (A.length - 1)) {
                result += ", ";
            }
        }
        return result + " ]";
    }

    public static int randomInt() {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(limit) + 1;
    }

    public static int[] arrayGenerator(final int len) {
        int[] A = new int[len];
        for (int i = 0; i < A.length; i++) {
            A[i] = randomInt();
        }
        return A;
    }

    public static String toString(char[] A) {
        String result = "[ ";
        for (int i = 0; i < A.length; i++) {
            result += A[i];
            if (i != (A.length - 1)) {
                result += ", ";
            }
        }
        return result + " ]";
    }
}
